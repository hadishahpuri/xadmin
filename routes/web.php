<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// --------- AUTH ROUTES ----------- //
Route::get('/login', function() {
    return view('templates.login');
})->name('login');

Route::post('/authenticate', 'UserController@Authenticate')->name('Aauthenticate');
Route::get('/logout', 'UserController@LogOut')->name('LogOut');
Route::get('/visit', 'UserController@NewVisit')->name('NewVisit');

Route::group(['middleware' => 'auth'], function () {

    // ---------- User Controller ------------- //
    Route::get('/', 'UserController@ShowHomePage')->name('ShowHomePage');
    Route::get('/home_data', 'UserController@HomePageData')->name('HomePageData');
    Route::post('/get_visits', 'UserController@GetVisits')->name('GetVisits');
    Route::get('/profile', 'UserController@UserProfile')->name('UserProfile');
    Route::post('/profile/edit/image', 'UserController@EditProfileImage')->name('EditProfileImage');
    Route::post('/profile/update/image', 'UserController@UpdateProfileImage')->name('UpdateProfileImage');
    Route::post('/profile/update', 'UserController@UpdateUser')->name('UpdateUser');

    // ---------- Customers Controller ------------- //
    Route::get('/customers', 'CustomerController@index')->name('CustomerIndex');
    Route::post('/customers/data', 'CustomerController@GetCustomers')->name('GetCustomers');
    Route::post('/customers/search', 'CustomerController@SearchCustomers')->name('SearchCustomers');
    Route::post('/customers/update_seen_status', 'CustomerController@UpdateCustomersSeenStatus')->name('UpdateCustomersSeenStatus');
    Route::post('/customers/new/view', 'CustomerController@NewCustomerView')->name('NewCustomerView');
    Route::post('/customers/new', 'CustomerController@RegisterNewCustomer')->name('RegisterNewCustomer');
    Route::post('/customers/active', 'CustomerController@ActivateCustomer')->name('ActivateCustomer');
    Route::post('/customers/deactive', 'CustomerController@DeactiveCustomer')->name('DeactiveCustomer');
    Route::post('/customers/details', 'CustomerController@CustomerMoreDetails')->name('CustomerMoreDetails');
    Route::get('/newsletter/view', 'CustomerController@NewNewsLetterView')->name('NewNewsLetterView');
    Route::post('/newsletter/new', 'CustomerController@NewNewsLetter')->name('NewNewsLetter');

    // ---------- Products Controller ------------- //
    Route::get('/products', 'ProductController@index')->name('ProductIndex');
    Route::post('/products/data', 'ProductController@GetProducts')->name('GetProducts');
    Route::get('/products/new/view', 'ProductController@NewProductView')->name('NewProductView');
    Route::post('/products/new', 'ProductController@NewProduct')->name('NewProduct');
    Route::post('/products/special_offer', 'ProductController@SpecialOffer')->name('SpecialOffer');
    Route::get('/products/edit/view/{product}', 'ProductController@EditProductView')->name('EditProductView');
    Route::post('/products/update', 'ProductController@UpdateProduct')->name('UpdateProduct');
    Route::post('/products/activate', 'ProductController@ActivateProduct')->name('ActivateProduct');


    // ---------- cats Controller ------------- //
    Route::get('/cats', 'ProductController@CatsIndex')->name('CatsIndex');
    Route::post('/cats/data', 'ProductController@GetCats')->name('GetCats');
    Route::get('/cats/new/view', 'ProductController@NewCatView')->name('NewCatView');
    Route::post('/cats/new', 'ProductController@NewCat')->name('NewCat');
    Route::get('/cats/edit/view/{product}', 'ProductController@EditCatView')->name('EditCatView');
    Route::post('/cats/update', 'ProductController@UpdateCat')->name('UpdateCat');
    Route::post('/cats/activate', 'ProductController@ActivateCat')->name('ActivateCat');


    // ---------- OrderController Controller ------------- //
    Route::get('/orders', 'OrderController@index')->name('OrdersIndex');
    Route::post('/orders/data', 'OrderController@GetOrdersHomeData')->name('GetOrdersHomeData');
    Route::post('/orders/change_status', 'OrderController@ChangeOrderStatus')->name('ChangeOrderStatus');
    Route::get('/orders/new/view', 'OrderController@NewOrderView')->name('NewOrderView');
    Route::post('/orders/new', 'OrderController@NewOrder')->name('NewOrder');
    Route::post('/orders/update_seen_status', 'OrderController@UpdateSeenStatus')->name('UpdateSeenStatus');

    // ---------- Message Controller ------------- //
    Route::get('/messages', 'MessageController@Index')->name('MessagesIndex');
    Route::post('/messages/data', 'MessageController@GetMessages')->name('GetMessages');
    Route::post('/messages/update_status', 'MessageController@UpdateMessageSeenStatus')->name('UpdateMessageSeenStatus');
    Route::post('/messages/reply_message/view', 'MessageController@ReplyMessageView')->name('ReplyMessageView');
    Route::post('/messages/new_message', 'MessageController@NewMessage')->name('NewMessage');
    Route::post('/messages/activate', 'MessageController@ActivateMessage')->name('ActivateMessage');

    // ---------- News Controller ------------- //
    Route::get('/news', 'NewsController@index')->name('NewsIndex');
    Route::post('/news/homeData', 'NewsController@GetNewsHomeData')->name('GetNewsHomeData');
    Route::get('/news/new/view', 'NewsController@NewNewsView')->name('NewNewsView');
    Route::post('/news/new', 'NewsController@NewNews')->name('NewNews');
    Route::get('/news/edit/view/{news}', 'NewsController@EditNewsView')->name('EditNewsView');
    Route::post('/news/update', 'NewsController@UpdateNews')->name('UpdateNews');
    Route::post('/news/activate', 'NewsController@ActivateNews')->name('ActivateNews');

    // ---------- Settings Controller ------------- //
    Route::get('/settings', 'SettingsController@index')->name('SettingsIndex');
    Route::post('/settings/homeData', 'SettingsController@SettingsHomeData')->name('SettingsHomeData');
    Route::post('/settings/update', 'SettingsController@UpdatePages')->name('UpdatePages');

    // ---------- Transactions Controller ------------- //
    Route::get('/transactions', 'TransactionController@Index')->name('TransactionIndex');
    Route::post('/transactions/data', 'TransactionController@GetTransactions')->name('GetTransactions');
    Route::post('/transactions/change_status', 'TransactionController@ChangeStatus')->name('ChangeStatus');
    Route::post('/transactions/update_seen_status', 'TransactionController@UpdateSeenStatus')->name('UpdateSeenStatus');
});
