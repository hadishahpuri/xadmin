@extends('layouts.app')
@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>

    <div class="card-body">
        <form action="{{ route('NewNewsLetter') }}" method="POST" class="formblk" id="frmNewNewsLetter" name="frmNewNewsLetter">
            <h6 class="heading-small text-muted mb-4U">خبرنامه جدید</h6>
            <div class="pl-lg-4">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-username">عنوان</label>
                            <input type="text" id="input-username" name="title" class="form-control form-control-alternative" placeholder="عنوان">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="type">نوع ارسال</label>
                            <select class="form-control form-control-alternative" name="type" id="type">
                                <option value="all">همه مشتری ها</option>
                                <option value="newsLetter">فقط لیست خبرنامه</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label" for="input-email">متن خبرنامه</label>
                            <textarea type="text" id="description" name="description" class="form-control form-control-alternative"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-info" id="NewNewsLetterBTN">ثبت</button>
            <hr class="my-4" />
        </form>
    </div>

    @include('layouts.footer')
@endsection
@push('newscript')
    <script>

        CKEDITOR.replace( 'description' );

        $('#NewNewsLetterBTN').click(function () {
            ShowLoader()
            $('#frmNewNewsLetter').ajaxForm(function (res) {
                HideLoader();
                if (res.msg === 'success') {
                    ShowMessage('success', 'با موفقیت ارسال شد', false, false);
                }
            });
        })

    </script>
@endpush

