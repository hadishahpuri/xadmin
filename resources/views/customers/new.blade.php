<div class="card-body" id="customerPopUp">
    <form action="{{ route('RegisterNewCustomer') }}" method="POST" id="frmRegisterNewCustomer" name="frmRegisterNewCustomer">
        <h6 class="heading-small text-muted mb-4U">ثبت مشتری جدید</h6>
        <div class="pl-lg-4">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">نام</label>
                        <input type="text" id="input-username" name="name" class="form-control form-control-alternative" placeholder="نام">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">نام خانوادگی</label>
                        <input type="text" id="input-email" name="last_name" class="form-control form-control-alternative" placeholder="نام خانوادگی">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">کد پستی</label>
                        <input type="text" id="input-username" name="postal_code" class="form-control form-control-alternative" placeholder="کد پستی">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">ایمیل</label>
                        <input type="email" id="input-email" name="email" class="form-control form-control-alternative" placeholder="ایمیل">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">موبایل</label>
                        <input type="text" id="input-first-name" name="cellphone" class="form-control form-control-alternative" placeholder="موبایل">
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-info" id="registerCustomerBTN">ثبت</button>
        <hr class="my-4" />
    </form>
</div>

<script>
    if (screen.width < 900) {
        console.log('small screen');
        $('#customerPopUp').addClass('margin_top', '50px').addClass('margin_bottom', '50px');
    }
    $('#registerCustomerBTN').click(function () {
        ShowLoader()
        $('#frmRegisterNewCustomer').ajaxForm(function (res) {
            HideLoader();
            if (res.msg === 'success') {
                ShowMessage('success', 'با موفقیت ثبت شد', false, false);
                PrependNewCustomer(res.customer);
            }
        });
    })

    function PrependNewCustomer(customer) {
    $('#tblBody').prepend(`
    <tr>
        <th scope="row" data-field-id="${customer.id}">
            <div class="media align-items-center">
            <a href="#" class="avatar rounded-circle mr-3">
            <img alt="" id="customer_${customer.id}img" src="/${customer.hash_key}" onerror="DefaultImg(this)">
            </a>
            <div class="media-body">
            <span class="mb-0 text-sm">${customer.name} ${customer.last_name}</span>
            </div>
            </div>
            </th>
            <td>
            ${customer.cellphone}
            </td>
            <td id="rowStatus">
              <span class="badge badge-dot mr-4" id="rowStatusSpan">
                فعال
              </span>
            </td>
            <td>
            <div class="d-flex align-items-center">
            <span class="mr-2">0</span>
            </div>
            </td>
            <td class="text-right">
            <div class="dropdown">
            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
            <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="ActiveClickChecking(this)">فعال کردن</a>
            <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="DeActiveClickChecking(this)">غیرفعال کردن</a>
            <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="MoreDetails(this)">اطلاعات بیشتر</a>
        </div>
        </div>
        </td>
        </tr>
    `)
    }
</script>
