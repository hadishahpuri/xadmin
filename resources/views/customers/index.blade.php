@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6" id="customersCard">
                        <div class="card card-stats mb-4 mb-xl-0" id="newCustomerForm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">ثبت مشتری جدید</h5>
                                        <span class="h2 font-weight-bold mb-0" id="customersNum"></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6" id="customersCard">
                        <a href="{{ route('NewNewsLetterView') }}">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">خبرنامه</h5>
                                        <span class="h2 font-weight-bold mb-0" id="customersNum"></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-xl-6 order-xl-1">
                        <div class="card bg-secondary shadow">
                            <div class="card-header bg-white border-0">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h3 class="mb-0">جستجوی مشتری</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('SearchCustomers') }}" method="POST" id="frmSearchCustomers" name="SearchCustomers">
                                    <div class="pl-lg-4">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-username">نام خانوادگی</label>
                                                    <input type="text" id="inputLastName" name="last_name" class="form-control form-control-alternative">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-first-name">موبایل</label>
                                                    <input type="text" id="inputCellphone" name="cellphone" class="form-control form-control-alternative">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info" id="searchCustomersBtn">جستجو</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="SearchResult"></div>


    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">مشتری ها</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">نام و نام خانوادگی</th>
                                <th scope="col">موبایل</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تعداد سفارشات</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentCustomerId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var offset = 1;
        var ajaxInProgress = false;
        var customerIds = [];

        if (first === true) {
            first = false;
            ShowLoader()
            CallAjaxFunc('/customers/data', {offset:offset}, CustomersTblGenerator)
        }

        $('#searchCustomersBtn').click(function (e) {
            e.preventDefault()
            if ( !$('#inputLastName').val() && !$('#inputCellphone').val()) {
                console.log('empty search')
            }else {
                ShowLoader()
                CallAjaxFunc('{{ route('SearchCustomers') }}', {cellphone:$('#inputCellphone').val(),last_name:$('#inputLastName').val()}, CustomerSearchResultGenerator)
            }
        })


        function CustomerSearchResultGenerator(res) {
            HideLoader()
            first = false
            customerIds = [];
            $('#SearchResult').empty()


            if (res.customers.length === 0) {
                $('#SearchResult').append(`
                    <div class="alert alert-warning text-center" id="emptySearchResult" style="margin-bottom: 200px;">هیچ نتیجه ای یافت نشد</div>
                `)
            }else {

                $('#SearchResult').append(`
                    <div class="container-fluid mt--7" style="margin-bottom: 200px;">
                        <div class="row mt-5" id="MT5">
                            <div class="col">
                                <div class="card bg-default shadow">
                                    <div class="card-header bg-transparent border-0">
                                        <div class="close_table" id="closeTblSearch"></div>
                                        <h3 class="text-white mb-0">نتیجه جستجو</h3>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table align-items-center table-dark table-flush"  style="background-color: #128083;">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">نام و نام خانوادگی</th>
                                                <th scope="col">موبایل</th>
                                                <th scope="col">وضعیت</th>
                                                <th scope="col">تعداد سفارشات</th>
                                                <th scope="col"></th>
                                            </tr>
                                            </thead>
                                            <tbody id="SearchResTblBody">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `)

                $('#closeTblSearch').click(function () {
                    console.log('table closed')
                    $('#SearchResult').empty()
                })

                var customers = res.customers;
                customers.forEach(customer => {

                    $('#SearchResTblBody').append(`
                 <tr data-field-customer-id="${customer.id}">
                          <th scope="row">
                                <div class="media align-items-center">
                                    <a href="#!" class="avatar rounded-circle mr-3">
                                        <img alt="" id="customer_${customer.id}img" src="${customer.hash_key}" onerror="DefaultImg(this)">
                                    </a>
                                    <div class="media-body">
                                        <span class="mb-0 text-sm">${customer.name} ${customer.last_name}</span>
                                    </div>
                                </div>
                            </th>
                            <td>
                                ${customer.cellphone}
                            </td>
                            <td id="rowStatus">
                              <span class="badge badge-dot mr-4" id="rowStatusSpan">
                                ${customer.row_status}
                              </span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${customer.orders_count}</span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="ActiveClickChecking(this)">فعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="DeActiveClickChecking(this)">غیرفعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="MoreDetails(this)">اطلاعات بیشتر</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)
                })
            }
        }


        function CustomersTblGenerator(res) {
            HideLoader()
            first = false
            customerIds = [];
            var customers = res.customers;
            customers.forEach(customer => {
                $('#tblBody').append(`
                 <tr data-field-customer-id="${customer.id}">
                          <th scope="row">
                                <div class="media align-items-center">
                                    <a href="#!" class="avatar rounded-circle mr-3">
                                        <img alt="" id="customer_${customer.id}img" src="${customer.hash_key}" onerror="DefaultImg(this)">
                                    </a>
                                    <div class="media-body">
                                        <span class="mb-0 text-sm">${customer.name} ${customer.last_name}</span>
                                    </div>
                                </div>
                            </th>
                            <td>
                                ${customer.cellphone}
                            </td>
                            <td id="rowStatus">
                              <span class="badge badge-dot mr-4" data-field-customer-status="${customer.id}">

                              </span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${customer.orders_count}</span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="ActiveClickChecking(this)">فعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="DeActiveClickChecking(this)">غیرفعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${customer.id}" onclick="MoreDetails(this)">اطلاعات بیشتر</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)
                var customerStatus = $('[data-field-customer-status ="'+ customer.id +'"]');
                if (customer.row_status === 'active') {
                    customerStatus.text('فعال');
                }else if (customer.row_status === 'suspended') {
                    customerStatus.text('غیر فعال');
                }

                ajaxInProgress = false;
                if (customer.new === 1) {
                    customerIds.push(customer.id);
                }
            })

            if (res.customers.length === 1) {
                $('#tblBody').children('tr').css("height", "100px")
            }

            offset = res.offset;
            if (customerIds.length > 0) {
                CallAjax('/customers/update_seen_status', {customers:customerIds})
            }
        }

        console.log(offset)
        $(function () {
            $(window).scroll(function () {
                    // if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    //     console.log('scrolled for ajax')
                    //     no = no + 1;
                    //     CallAjaxFunc('http://xadmin.local/customers/data', {offset:offset}, CustomersTblGenerator)
                    // }
                // if ($('#dataLoad').visible(true)) {
                //         console.log('scrolled for ajax')
                //         CallAjaxFunc('http://xadmin.local/customers/data', {offset:offset}, CustomersTblGenerator)
                // }

                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('/customers/data', {offset:offset}, CustomersTblGenerator)
                    }
                }

            })
        })

    })

    $('#newCustomerForm').click(function () {
        ShowPopUp("new customer", "{{url(route("NewCustomerView"))}}",{id:'id',readonly:true})
    })



    function ActiveClickChecking(obj) {
        $('#currentCustomerId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,ActiveClickConfirm);
    }

    function ActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/customers/active', {id:$('#currentCustomerId').val()}, ActiveSuccess)
    }

    function ActiveSuccess(res) {
        console.log(res.customer.id)
        if (res.msg === 'success') {
            HideLoader()
            ShowMessage('success', 'با موفقیت فعال شد', false, false);
            var customerStatus = $('[data-field-customer-status ="'+ res.customer.id +'"]');
            customerStatus.text('فعال')
        }
    }




    function DeActiveClickChecking(obj) {
        $('#currentCustomerId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,DeActiveClickConfirm);
    }

    function DeActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/customers/deactive', {id:$('#currentCustomerId').val()}, DeActiveSuccess)
    }

    function DeActiveSuccess(res) {
        console.log(res.customer.id)
        if (res.msg === 'success') {
            HideLoader()
            ShowMessage('success', 'با موفقیت غیرفعال شد', false, false);
            var customerStatus = $('[data-field-customer-status ="'+ res.customer.id +'"]');
            customerStatus.text('غیر فعال')
        }
    }


    function MoreDetails(obj) {
        var id = $(obj).attr('data-field-id');
        ShowPopUp("اطلاعات مشتری", "{{url(route("CustomerMoreDetails"))}}",{id:id,readonly:true})
    }
</script>
@endpush
