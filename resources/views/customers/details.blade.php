<div class="card-body" id="customerPopUp" >
        <h6 class="heading-small text-muted mb-4U">اطلاعات مشتری</h6>
        <div class="pl-lg-4" id="dialogBody">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">نام</label>
                        <input type="text" id="input-username" name="name" class="form-control form-control-alternative" value="{{ $customer->name }}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">نام خانوادگی</label>
                        <input type="text" id="input-email" name="last_name" class="form-control form-control-alternative" value="{{ $customer->last_name }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">کد پستی</label>
                        <input type="text" id="input-username" name="postal_code" class="form-control form-control-alternative" value="{{ $customer->postal_code }}">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">ایمیل</label>
                        <input type="email" id="input-email" name="email" class="form-control form-control-alternative" value="{{ $customer->email }}">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">موبایل</label>
                        <input type="text" id="input-first-name" name="cellphone" class="form-control form-control-alternative" value="{{ $customer->cellphone }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">آدرس</label>
                        <input type="text" id="input-first-name" name="address" class="form-control form-control-alternative" value="{{ $customer->address }}">
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4" />
        <div class="pl-lg-4">
            <div class="row">
                <div class="container-fluid mt--7">
                    <div class="row mt-5" id="MT5">
                        <div class="col">
                            <div class="card bg-default shadow">
                                <div class="card-header bg-transparent border-0">
                                    <h3 class="text-white mb-0">سفارشات</h3>
                                </div>
                                <div class="table-responsive">
                                    <table class="table align-items-center table-dark table-flush">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">مشتری</th>
                                            <th scope="col">محصولات</th>
                                            <th scope="col">مبلغ کل</th>
                                            <th scope="col">وضعیت</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tblBody">
                                        @foreach($orders as $order)
                                            <tr data-field-order-id="{{ $order->id }}">
                                                <td>
                                                    {{ $order->name . ' ' . $order->last_name }}
                                                </td>
                                                <td data-field-order-products="{{ $order->id }}">
                                                    <div class="avatar-group" data-field-avatars="{{ $order->id }}">
                                                        @foreach($order->info as $item)
                                                            <a href="#" class="avatar avatar-sm" title="{{ $item->title . ',' . ' price : ' . $item->price . ',' . $item->count }}" data-toggle="tooltip">
                                                                <img src="{{ $item->hash_1}}" onerror="DefaultImg(this)" class="rounded-circle">
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <span class="mr-2">{{ $order->price }}</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <span class="mr-2" data-field-order-status="{{ $order->id }}">
                                                            @if($order->status == 'payed')  پرداخت شد @elseif($order->status == 'delivered') تحویل داده شد @elseif($order->status == 'canceled') لغو شد @endif
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="text-right">
                                                    <div class="dropdown">
                                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                            <h6>تغییر وضعیت به</h6>
                                                            <a class="dropdown-item" href="#" data-field-id="{{ $order->id }}" onclick="ChangeOrderStatusToPayed(this)">پرداخت شد</a>
                                                            <a class="dropdown-item" href="#" data-field-id="{{ $order->id }}" onclick="ChangeOrderStatusToDelivered(this)">تحویل داده شد</a>
                                                            <a class="dropdown-item" href="#" data-field-id="{{ $order->id }}" onclick="ChangeOrderStatusToCanceled(this)">لغو شد</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<input type="hidden" id="currentOrderId">
<script>
    $(document).ready(function () {
        $('[data-toggle ="tooltip"]').tooltip();
    })
    if (screen.width < 900) {
        console.log('small screen');
        $('#dialogBody').css("margin-top", "100px");
    }


    function ChangeOrderStatusToDelivered(obj) {
        ShowLoader();
        CallAjaxFunc('/orders/change_status', {id:$(obj).attr('data-field-id'), status:"delivered"}, StatusToDeliveredSuccess)
    }

    function StatusToDeliveredSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var order = $('[data-field-order-status ="'+ res.order.id +'"]');
            order.text('تحویل داده شد');
        }
    }

    function ChangeOrderStatusToPayed(obj) {
        ShowLoader();
        CallAjaxFunc('/orders/change_status', {id:$(obj).attr('data-field-id'), status:"payed"}, StatusToPayedSuccess)
    }

    function StatusToPayedSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var order = $('[data-field-order-status ="'+ res.order.id +'"]');
            order.text('پرداخت شد');
        }
    }


    function ChangeOrderStatusToCanceled(obj) {
        ShowLoader();
        CallAjaxFunc('/orders/change_status', {id:$(obj).attr('data-field-id'), status:"canceled"}, StatusToCanceledSuccess)
    }

    function StatusToCanceledSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var order = $('[data-field-order-status ="'+ res.order.id +'"]');
            order.text('لغو شد');
        }
    }

</script>
