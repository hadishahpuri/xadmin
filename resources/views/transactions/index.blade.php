@extends('layouts.app')


@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">تراکنش ها</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">مشتری</th>
                                <th scope="col">مبلغ کل</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentTransactionId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var transactionIds = [];
        var offset = 1;
        var ajaxInProgress = false;

        if (first === true) {
            first = false;
            ShowLoader()
            CallAjaxFunc('/transactions/data', {offset:offset}, TransactionsTableGenerator)
        }

        function TransactionsTableGenerator(res) {
            HideLoader()
            first = false
            var transactions = res.transactions;
            transactions.forEach(trans => {
                $('#tblBody').append(`
                 <tr data-field-trans-id="${trans.id}">
                            <td>
                                ${trans.name + ' ' + trans.last_name}
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${trans.price}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-trans-status="${trans.id}"></span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${trans.p_date}</span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <h6>change status to</h6>
                                        <a class="dropdown-item" href="#" data-field-id="${trans.id}" onclick="ChangeTransStatusToPayed(this)">payed</a>
                                        <a class="dropdown-item" href="#" data-field-id="${trans.id}" onclick="ChangeTransStatusToCanceled(this)">canceled</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)
                if (trans.new === 1) {
                    transactionIds.push(trans.id);
                }

                var transaction = $('[data-field-trans-status ="'+ trans.id +'"]');
                if (trans.status === 'none') {
                    transaction.text('هیچ کدام')
                }else if (trans.status === 'payed') {
                    transaction.text('پرداخت شده')
                } else if (trans.status === 'canceled') {
                    transaction.text('لغو شده')
                }
            })
            if (res.transactions.length === 1) {
                $('#tblBody').children('tr').css("height", "100px")
            }
            offset = res.offset;
            first = false;

            if (transactionIds.length > 0) {
                CallAjax('/transactions/update_seen_status', {transactions:transactionIds})
            }
        }

        console.log(offset)
        $(function () {
            $(window).scroll(function () {

                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('/orders/data', {offset:offset}, TransactionsTableGenerator)
                    }
                }

            })
        })


    })
    $('#orderCard').click(function () {
        location.href = '/orders/new/view';
    })



    function ChangeTransStatusToPayed(obj) {
        ShowLoader();
        CallAjaxFunc('/transactions/change_status', {id:$(obj).attr('data-field-id'), status:"payed"}, StatusToPayedSuccess)
    }

    function StatusToPayedSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var trans = $('[data-field-trans-status ="'+ res.transaction.id +'"]');
            trans.text('پرداخت شد');
        }
    }


    function ChangeTransStatusToCanceled(obj) {
        ShowLoader();
        CallAjaxFunc('/transactions/change_status', {id:$(obj).attr('data-field-id'), status:"canceled"}, StatusToCanceledSuccess)
    }

    function StatusToCanceledSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var trans = $('[data-field-trans-status ="'+ res.transaction.id +'"]');
            trans.text('لغو شد');
        }
    }



</script>
@endpush
