@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>

    <div class="card-body" id="customerPopUp">
        <form action="{{ route('UpdateProduct') }}" method="POST" class="formblk" id="frmUpdateProduct" name="frmUpdateProduct">
            <h6 class="heading-small text-muted mb-4U">ویرایش محصول : {{ $product->title }}</h6>
            <div class="pl-lg-4">
                <div class="row">
                    <div class="uploadPicture mainPic">
                        <div class="inner">
                            <label for="hash_1" class="uploadBtn"></label>
                            <img src="{{ $product->hash_1 }}" alt="" class="previewPic" id="previewPic_hash_1">
                            <input type="file" name="hash_1" id="hash_1" onchange="GetFileInputForCompress(this);" accept="image/jpeg, image/png" />
                        </div>
                    </div>
                    <div class="uploadPicture mainPic">
                        <div class="inner">
                            <label for="hash_2" class="uploadBtn"></label>
                            <img src="{{ $product->hash_2 }}" alt="" class="previewPic" id="previewPic_hash_2">
                            <input type="file" name="hash_2" id="hash_2" onchange="GetFileInputForCompress(this);" accept="image/jpeg, image/png" />
                        </div>
                    </div>
                    <div class="uploadPicture mainPic">
                        <div class="inner">
                            <label for="hash_3" class="uploadBtn"></label>
                            <img src="{{ $product->hash_3 }}" alt="" class="previewPic" id="previewPic_hash_3">
                            <input type="file" name="hash_3" id="hash_3" onchange="GetFileInputForCompress(this);" accept="image/jpeg, image/png" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-username">عنوان</label>
                            <input type="text" id="input-username" name="title" value="{{ $product->title }}" class="form-control form-control-alternative" placeholder="عنوان">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-email">دسته مربوطه</label>
                            <select class="form-control form-control-alternative" name="category_id" id="category_id">
                                @foreach($cats as $cat)
                                    <option value="{{ $cat->id }}" @if($product->category_id == $cat->id) selected @endif>{{ $cat->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-username">قیمت</label>
                            <input type="number" id="price" name="price" value="{{ $product->price }}" class="form-control form-control-alternative" placeholder="قیمت">
                        </div>
                    </div>
                    <div class="col-lg-6 text-center">
                        <div class="form-group">
                            <label class="form-control-label" for="special_offer">پیشنهاد ویژه</label>
                            <input type="checkbox" id="special_offer" name="special_offer" @if($product->special_offer == 1) checked @endif>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label" for="page_desc">توضیحات صفحه</label>
                            <input type="text" id="page_desc" name="page_desc" class="form-control form-control-alternative" value="{{ $product->page_desc }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label" for="input-email">توضیحات</label>
                            <textarea type="text" id="description" name="description" class="form-control form-control-alternative" placeholder="توضیحات">{!! $product->description !!}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{ $product->id }}">
            <button type="submit" class="btn btn-info" id="UpdateProductBtn">بروزرسانی</button>
            <hr class="my-4" />
        </form>
    </div>

    @include('layouts.footer')
@endsection
@push('newscript')
    <script>
        CKEDITOR.replace( 'description' );

        $('#UpdateProductBtn').click(function () {
            ShowLoader()
            $('#frmUpdateProduct').ajaxForm(function (res) {
                HideLoader();
                if (res.msg === 'success') {
                    ShowMessage('success', 'با موفقیت انجام شد', false, false);
                }
            });
        })





        var fileInput = document.getElementById('hash_1');

        var max_width = 700;
        var max_height = 700;

        var preview = document.getElementById('previewPic1');

        var form = document.getElementById('frmUpdateProduct');


        function processFile(file) {

            if( !( /image/i ).test( file.type ) )
            {
                alert( "File "+ file.name +" is not an image." );
                return false;
            }

            // read the files
            var reader = new FileReader();
            reader.readAsArrayBuffer(file);

            reader.onload = function (event) {
                // blob stuff
                var blob = new Blob([event.target.result]); // create blob...
                window.URL = window.URL || window.webkitURL;
                var blobURL = window.URL.createObjectURL(blob); // and get it's URL

                // helper Image object
                var image = new Image();
                image.src = blobURL;
                //preview.appendChild(image); // preview commented out, I am using the canvas instead
                image.onload = function() {
                    // have to wait till it's loaded
                    var resized = resizeMe(image); // send it to canvas
                    var newinput = document.createElement("input");
                    newinput.type = 'hidden';
                    newinput.name = fileInput.name + "_blob";
                    preview.src = resized;
                    newinput.value = resized; // put result from canvas into new hidden input
                    form.appendChild(newinput);
                }
            };
        }

        function readFiles(files) {

            // remove the existing canvases and hidden inputs if user re-selects new pics
            var existinginputs = document.getElementsByName(fileInput.name + "_blob");
            var existingcanvases = document.getElementsByName(fileInput.name + "_canvas");
            while (existinginputs.length > 0) { // it's a live list so removing the first element each time
                // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
                form.removeChild(existinginputs[0]);
                preview.removeChild(existingcanvases[0]);
            }

            for (var i = 0; i < files.length; i++) {
                processFile(files[i]); // process each file at once
            }
            fileInput.value = ""; //remove the original files from fileinput
            // TODO remove the previous hidden inputs if user selects other files
        }

        // this is where it starts. event triggered when user selects files
        // fileInput.onchange = function(){
        //     if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
        //         alert('The File APIs are not fully supported in this browser.');
        //         return false;
        //     }
        //     readFiles(fileInput.files);
        // }


        function GetFileInputForCompress(input) {
            fileInput = document.getElementById(input.name);
            preview = document.getElementById("previewPic_" + input.name)
            if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
                alert('The File APIs are not fully supported in this browser.');
                return false;
            }
            readFiles(fileInput.files);
        }

        // === RESIZE ====

        function resizeMe(img) {

            var canvas = document.createElement('canvas');

            var width = img.width;
            var height = img.height;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                    //height *= max_width / width;
                    height = Math.round(height *= max_width / width);
                    width = max_width;
                }
            } else {
                if (height > max_height) {
                    //width *= max_height / height;
                    width = Math.round(width *= max_height / height);
                    height = max_height;
                }
            }

            // resize the canvas and draw the image data into it
            canvas.width = width;
            canvas.height = height;
            canvas.setAttribute("name", fileInput.name + "_canvas")
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);

            preview.append(canvas); // do the actual resized preview

            return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }


    </script>
@endpush
