@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6" id="customersCard">
                        <div class="card card-stats mb-4 mb-xl-0" id="newProductForm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">ثبت محصول</h5>
                                        <span class="h2 font-weight-bold mb-0" id="customersNum"></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">محصولات</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">عنوان</th>
                                <th scope="col">توضیحات</th>
                                <th scope="col">قیمت</th>
                                <th scope="col">تعداد بازدید</th>
                                <th scope="col">پیشنهاد ویژه</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تعداد سفارشات</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentProductId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var offset = 1;
        var ajaxInProgress = false;

        if (first === true) {
            ShowLoader()
            CallAjaxFunc('/products/data', {offset:offset}, ProductsTblGenerator)
        }

        function ProductsTblGenerator(res) {
            HideLoader()
            first = false
            var products = res.products;
            products.forEach(product => {
                $('#tblBody').append(`
                 <tr data-field-customer-id="${product.id}">
                          <th scope="row">
                                <div class="media align-items-center">
                                    <a href="#!" class="avatar rounded-circle mr-3" disabled>
                                        <img alt="" id="customer_${product.id}img" src="${product.hash_1}" onerror="DefaultImg(this)" style="border-radius: 50%;">
                                    </a>
                                    <div class="media-body">
                                        <span class="mb-0 text-sm">${product.title}</span>
                                    </div>
                                </div>
                            </th>
                            <td id="productDesc">
                                توضیحات محصول فقط در صفحه ویرایش قابل مشاهده است
                            </td>
                            <td id="rowStatus">
                              <span class="badge badge-dot mr-4" id="rowStatusSpan">
                                ${product.price}
                              </span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${product.view_count}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center" data-field-special-id="${product.id}" id="SpecialOfferDiv">
                                        <input type="checkbox" id="SpecialOfferInput" data-field-check-id="${product.id}" onclick="SpecialOfferChange(this)">
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-product-status="${product.id}"></span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${product.orders_count}</span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#" data-field-id="${product.id}" onclick="ActiveClickChecking(this)">فعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${product.id}" onclick="DeActiveClickChecking(this)">غیرفعال کردن</a>
                                        <a class="dropdown-item" href="/products/edit/view/${product.id}" data-field-id="${product.id}">ویرایش</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)
                var specialProduct = $('[data-field-special-id ="'+ product.id +'"]');
                console.log(product.special_offer);
                console.log(specialProduct.children('#SpecialOfferInput'));
                if (product.special_offer === 1) {
                    specialProduct.children('#SpecialOfferInput').attr('checked', true);
                }else {
                    specialProduct.children('#SpecialOfferInput').removeAttr('checked');
                }

                var statusProduct = $('[data-field-product-status ="'+ product.id +'"]');
                if (product.row_status === 'active') {
                    statusProduct.text('فعال')
                }else if (product.row_status === 'suspended') {
                    statusProduct.text('غیر فعال')
                }
            })

            if (res.products.length === 1) {
                $('#tblBody').children('tr').css("height", "100px")
            }
            ajaxInProgress = false;

            offset = res.offset;
        }

        console.log(offset)
        $(function () {
            $(window).scroll(function () {

                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('/products/data', {offset:offset}, ProductsTblGenerator)
                    }
                }

            })
        })

    })

    $('#newProductForm').click(function () {
        location.href = '/products/new/view';
    })


    function SpecialOfferChange(obj) {
        console.log($(obj).attr('data-field-check-id'));
        ShowLoader();
        CallAjaxFunc('/products/special_offer', {id:$(obj).attr('data-field-check-id')}, SpecialOfferSuccess)
    }

    function SpecialOfferSuccess(res) {
        HideLoader();
        if (res.msg === 'error') {
            ShowMessage("error",res.error,false,false)
        }
        var sProduct = $('[data-field-check-id ="'+ res.product.id +'"]');
        if (res.product.special_offer === 1) {
            sProduct.attr('checked', true);
        }else {
            sProduct.removeAttr('checked');
        }
    }


    function ActiveClickChecking(obj) {
        $('#currentProductId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,ActiveClickConfirm);
    }

    function ActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/products/activate', {id:$('#currentProductId').val(), type:"active"}, ActiveSuccess)
    }

    function ActiveSuccess(res) {
        HideLoader();
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت فعال شد', false, false);
            var product = $('[data-field-product-status ="'+ res.product.id +'"]');
            product.text('فعال');
        }
    }

    function DeActiveClickChecking(obj) {
        $('#currentProductId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,DeActiveClickConfirm);
    }

    function DeActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/products/activate', {id:$('#currentProductId').val(), type:"deActive"}, DeActiveSuccess)
    }

    function DeActiveSuccess(res) {
        HideLoader()
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت غیر فعال شد', false, false);
            var product = $('[data-field-product-status ="'+ res.product.id +'"]');
            product.text('غیرفعال');
        }
    }


    function EditProduct(obj) {
        {{--CallAjaxFunc('{{ route('EditProductView') }}', {id:$(obj).attr('data-field-id')}, EditSuccess)--}}
        $.ajax({
            type: "POST",
            url: "/products/edit/view",
            data: {id:$(obj).attr('data-field-id')}
        })
    }
</script>
@endpush
