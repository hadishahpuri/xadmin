@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>

    <div class="card-body" id="customerPopUp">
        <form action="{{ route('UpdateCat') }}" method="POST" class="formblk" id="frmUpdateCat" name="frmUpdateCat">
            <h6 class="heading-small text-muted mb-4U">ویرایش دسته : {{ $cat->title }}</h6>
            <div class="pl-lg-4">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label" for="input-username">عنوان</label>
                            <input type="text" id="input-username" name="title" value="{{ $cat->title }}" class="form-control form-control-alternative" placeholder="عنوان">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label" for="input-email">توضیحات</label>
                            <textarea type="text" id="description" name="description" class="form-control form-control-alternative" placeholder="توضیحات">{!! $cat->description !!}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{ $cat->id }}">
            <button type="submit" class="btn btn-info" id="UpdateCatBtn">بروزرسانی</button>
            <hr class="my-4" />
        </form>
    </div>

    @include('layouts.footer')
@endsection
@push('newscript')
    <script>
        $('#UpdateCatBtn').click(function () {
            ShowLoader()
            $('#frmUpdateCat').ajaxForm(function (res) {
                HideLoader();
                if (res.msg === 'success') {
                    ShowMessage('success', 'با موفقیت انجام شد', false, false);
                }
            });
        })

    </script>
@endpush
