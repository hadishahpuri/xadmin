@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6" id="catCard">
                        <div class="card card-stats mb-4 mb-xl-0" id="newCatForm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">ثبت دسته بندی جدید</h5>
                                        <span class="h2 font-weight-bold mb-0" id="customersNum"></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">دسته های محصولات</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">عنوان</th>
                                <th scope="col">توضیحات</th>
                                <th scope="col">تعداد محصولات</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentCatId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var offset = 1;
        var ajaxInProgress = false;

        if (first === true) {
            ShowLoader()
            CallAjaxFunc('/cats/data', {offset:offset}, CatsTblGenerator)
        }

        function CatsTblGenerator(res) {
            HideLoader()
            first = false
            var cats = res.cats;
            cats.forEach(cat => {
                $('#tblBody').append(`
                 <tr data-field-cat-id="${cat.id}">
                        <td id="catTitle">
                            ${cat.title}
                        </td>
                        <td id="catDesc" style="overflow: hidden; overflow-wrap: normal; max-width: 100px;">
                            ${cat.description}
                        </td>
                        <td>
                            <div class="d-flex align-items-center">
                                <span class="mr-2">${cat.product_count}</span>
                            </div>
                        </td>
                        <td id="rowStatus">
                          <span class="badge badge-dot mr-4" id="rowStatusSpan" data-field-cat-status="${cat.id}">
                            ${cat.row_status}
                          </span>
                        </td>
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="#" data-field-id="${cat.id}" onclick="ActiveClickChecking(this)">فعال کردن</a>
                                    <a class="dropdown-item" href="#" data-field-id="${cat.id}" onclick="DeActiveClickChecking(this)">غیرفعال کردن</a>
                                    <a class="dropdown-item" href="/cats/edit/view/${cat.id}" data-field-id="${cat.id}">ویرایش</a>
                                </div>
                            </div>
                        </td>
                    </tr>
            `)

                var statusCat = $('[data-field-cat-status ="'+ cat.id +'"]');
                if (cat.row_status === 'active') {
                    statusCat.text('فعال')
                }else if (cat.row_status === 'suspended') {
                    statusCat.text('غیر فعال')
                }
            })

            if (res.cats.length < 2) {
                $('#tblBody').children('tr').css("height", "100px")
            }
            ajaxInProgress = false;

            offset = res.offset;
        }

        console.log(offset)
        $(function () {
            $(window).scroll(function () {

                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('/cats/data', {offset:offset}, CatsTblGenerator)
                    }
                }

            })
        })

    })

    $('#newCatForm').click(function () {
        location.href = '/cats/new/view';
    })


    function ActiveClickChecking(obj) {
        $('#currentCatId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,ActiveClickConfirm);
    }

    function ActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/cats/activate', {id:$('#currentCatId').val(), type:"active"}, ActiveSuccess)
    }

    function ActiveSuccess(res) {
        HideLoader();
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت فعال شد', false, false);
            var cat = $('[data-field-cat-status ="'+ res.cat.id +'"]');
            cat.text('فعال');
        }
    }

    function DeActiveClickChecking(obj) {
        $('#currentCatId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,DeActiveClickConfirm);
    }

    function DeActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/cats/activate', {id:$('#currentCatId').val(), type:"deActive"}, DeActiveSuccess)
    }

    function DeActiveSuccess(res) {
        HideLoader()
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت غیر فعال شد', false, false);
            var cat = $('[data-field-cat-status ="'+ res.cat.id +'"]');
            cat.text('غیرفعال');
        }
    }


    function EditProduct(obj) {
        {{--CallAjaxFunc('{{ route('EditProductView') }}', {id:$(obj).attr('data-field-id')}, EditSuccess)--}}
        $.ajax({
            type: "POST",
            url: "/cats/edit/view",
            data: {id:$(obj).attr('data-field-id')}
        })
    }
</script>
@endpush
