@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>

<div class="card-body" id="customerPopUp">
    <form action="{{ route('NewCat') }}" method="POST" class="formblk" id="frmRegisterNewCat" name="frmRegisterNewCat">
        <h6 class="heading-small text-muted mb-4U">ثبت دسته بندی جدید</h6>
        <div class="pl-lg-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">عنوان</label>
                        <input type="text" id="input-username" name="title" class="form-control form-control-alternative" placeholder="عنوان">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">توضیحات</label>
                        <textarea type="text" id="description" name="description" class="form-control form-control-alternative"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-info" id="registerCatBTN">ثبت</button>
        <hr class="my-4" />
    </form>
</div>

@include('layouts.footer')
@endsection
@push('newscript')
<script>

    $('#registerCatBTN').click(function () {
        ShowLoader()
        $('#frmRegisterNewCat').ajaxForm(function (res) {
            HideLoader();
            if (res.msg === 'success') {
                ShowMessage('success', 'با موفقیت ثبت شد', false, false);
            }
        });
    })

</script>
@endpush
