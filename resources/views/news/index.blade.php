@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6" id="customersCard">
                        <div class="card card-stats mb-4 mb-xl-0" id="newNewsForm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">ثبت خبر جدید</h5>
                                        <span class="h2 font-weight-bold mb-0" id="customersNum"></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">اخبار</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">عنوان</th>
                                <th scope="col">توضیحات</th>
                                <th scope="col">تعداد بازدید</th>
                                <th scope="col">تعداد لایک</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentNewsId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var offset = 1;
        var ajaxInProgress = false;

        if (first === true) {
            ShowLoader()
            CallAjaxFunc('/news/homeData', {offset:offset}, NewsTblGenerator)
        }

        function NewsTblGenerator(res) {
            HideLoader()
            first = false
            var news = res.news;
            news.forEach(item => {
                $('#tblBody').append(`
                 <tr data-field-news-id="${item.id}">
                          <th scope="row">
                                <div class="media align-items-center">
                                    <a href="#!" class="avatar rounded-circle mr-3" disabled>
                                        <img alt="" id="news_${item.id}_img" src="${item.hash_1}" onerror="DefaultImg(this)" style="border-radius: 50%;">
                                    </a>
                                    <div class="media-body">
                                        <span class="mb-0 text-sm">${item.title}</span>
                                    </div>
                                </div>
                            </th>
                            <td id="productDesc">
                                توضیحات فقط در صفحه ی ویرایش قابل مشاهده است
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${item.view_count}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${item.like_count}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-news-status="${item.id}"></span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#" data-field-id="${item.id}" onclick="ActivateNewsClickChecking(this)">active</a>
                                        <a class="dropdown-item" href="#" data-field-id="${item.id}" onclick="deActivateNewsClickChecking(this)">deActive</a>
                                        <a class="dropdown-item" href="/news/edit/view/${item.id}" data-field-id="${item.id}">edit</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)

                var news = $('[data-field-news-status ="'+ item.id +'"]');
                if (item.row_status === 'active') {
                    news.text('فعال')
                }else if (item.row_status === 'suspended') {
                    news.text('غیرفعال')
                }

            })


            if (res.news.length === 1) {
                $('#tblBody').children('tr').css("height", "100px")
            }


            offset = res.offset;
            first = false;
        }

        console.log(offset)
        $(function () {
            $(window).scroll(function () {

                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('/news/homeData', {offset:offset}, NewsTblGenerator)
                    }
                }

            })
        })

    })

    $('#newNewsForm').click(function () {
        location.href = '/news/new/view';
    })



    function ActivateNewsClickChecking(obj) {
        $('#currentNewsId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,ActiveNewsClickConfirm);
    }

    function ActiveNewsClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/news/activate', {id:$('#currentNewsId').val(), type:"active"}, ActiveNewsSuccess)
    }

    function ActiveNewsSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت فعال شد', false, false);
            var news = $('[data-field-news-status ="'+ res.news.id +'"]');
            news.text('فعال');
        }
    }



    function deActivateNewsClickChecking(obj) {
        $('#currentNewsId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,deActiveNewsClickConfirm);
    }

    function deActiveNewsClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/news/activate', {id:$('#currentNewsId').val(), type:"deActive"}, deActiveNewsSuccess)
    }

    function deActiveNewsSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        } else {
            ShowMessage('success', 'با موفقیت غیر فعال شد', false, false);
            var news = $('[data-field-news-status ="'+ res.news.id +'"]');
            news.text('غیر فعال');
        }
    }



</script>
@endpush
