<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        پنل مدیریت
    </title>
    <!-- Favicon -->
    <link href="{{ asset('img/brand/favicon.png') }}" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="{{ asset('js/nucleo/css/nucleo.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="{{ asset('css/argon-dashboard.css?v=1.1.0') }}" rel="stylesheet" />


  <!--   Core   -->
  <script src="{{ asset('js/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('js/jquery/jquery.form.js') }}"></script>
  <script src="{{ asset('js/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!--   Optional JS   -->
  <!--   Argon JS   -->
  <script src="{{ asset('js/argon-dashboard.min.js?v=1.1.0') }}"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>


    </head>

<body class="bg-default">
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
      <div class="container px-4">
        <a class="navbar-brand" href="#">
          <img src="{{ asset('img/brand/white.png') }}" />
        </a>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">خوش آمدید!</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
              <form method="POST" action="{{ route('Aauthenticate') }}" name="frmAuthenticate" id="frmAuthenticate">
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="ایمیل" type="email" name="email" id="email">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="رمز" type="password" name="password" id="password">
                  </div>
                </div>
{{--                <div class="custom-control custom-control-alternative custom-checkbox">--}}
{{--                  <input class="custom-control-input" id="customCheckLogin" name="remember_token" type="checkbox">--}}
{{--                  <label class="custom-control-label" for=" customCheckLogin">--}}
{{--                    <span class="text-muted">Remember me</span>--}}
{{--                  </label>--}}
{{--                </div>--}}
                <div class="text-center">
                  <input type="submit" value="ورود" class="btn btn-primary my-4">
                </div>
              </form>
            </div>
          </div>
{{--          <div class="row mt-3">--}}
{{--            <div class="col-6">--}}
{{--              <a href="#" class="text-light"><small>Forgot password?</small></a>--}}
{{--            </div>--}}
{{--          </div>--}}
        </div>
      </div>
    </div>
    <footer class="py-5">
      <div class="container">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              © 2018 <a href="#!" class="font-weight-bold ml-1">X ADMIN</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

<div id="loading">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
            <div class="object"></div>
        </div>
    </div>
</div>
<div class="modal_box">
    <div class="innerbox">
        <div class="exit"></div>
        <div class="main_text">
            <div class="title">تیتر پاپ اپ</div>
            <div class="mohtava" id="mohtava">
                <div id="box_loading">
                    <div id="loading-center">
                        <div id="loading-center-absolute">
                            <div class="box_object" id="box_object_one"></div>
                            <div class="box_object" id="box_object_two"></div>
                            <div class="box_object" id="box_object_three"></div>
                            <div class="box_object" id="box_object_four"></div>
                            <div class="box_object" id="box_object_five"></div>
                            <div class="box_object" id="box_object_six"></div>
                            <div class="box_object" id="box_object_seven"></div>
                            <div class="box_object" id="box_object_eight"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="msgBox">
    <div class="innerbox">
        <div class="exit"></div>
        <div class="topSide"></div>
        <div class="bottomSide"></div>
        <div class="btns">
            <div class="btn confirmBtn">تایید</div>
            <div class="btn cancelBtn">لغو</div>
        </div>
    </div>
</div>
  <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error : function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 404) {
                HideLoader();
                ShowMessage('error','صفحه مورد نظر شما یافت نشد .',false,false);
            } else {
                HideLoader();
                ShowMessage('error','خطایی رخ داده است. لطفا مجددا تلاش نمایید .',false,false);
            }
        }
    });

    $(document).ready(function() {
        $("#frmAuthenticate").ajaxForm();
    });







$(document).ready(function() {
	$(window).scroll( function(){
		if ($(this).scrollTop() > 50) {
			$('.totop').fadeIn();
		} else {
			$('.totop').fadeOut();
		}
	});
	$('.totop').click(function() {
		$('html, body').animate({scrollTop : 0},1000);
		return false;
	});

	$(".replyBox .back").click(function(){
		$(".replyBox").fadeOut();
    });

    $(".modal_box .exit").click(function(){
		$(".modal_box").fadeOut();
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$(".replyBox").fadeOut();
            $(".msgTxt").fadeOut();
            $(".modal_box").fadeOut();
		}
	});
	$(".replyBox").click(function(e) {
	    if (e.target === this) {
	        $(".replyBox").fadeOut();
	    }
    });
    $(".modal_box").click(function(e) {
	    if (e.target === this) {
	        $(".modal_box").fadeOut();
	    }
	});
	$(".msgTxt").click(function(e) {
	    if (e.target === this) {
	        $(".msgTxt").fadeOut();
	    }
	});

	$(".changepwBox").click(function(e) {
	    if (e.target === this) {
	        $(".changepwBox").fadeOut();
	    }
	});

	$(".changepwBox .exit").click(function(){
		$(".changepwBox").fadeOut();
	});

	$(".msgTxt .exit").click(function(){
		$(".msgTxt").fadeOut();
	});

	$(".userInfo .exit").click(function(){
		$(".userInfo").fadeOut();
	});
	$(".userInfo").click(function(e) {
	    if (e.target === this) {
	        $(".userInfo").fadeOut();
	    }
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$(".userInfo").fadeOut();
		}
	});
});

function ShowMessage(type,msg,btns,reload,confirmBtnfunc) {

    $('.msgBox .bottomSide').html(msg);

    if (btns==false)
		$('.msgBox .btns').hide();
	else{
		$('.msgBox .btns').show();
		$('.msgBox .confirmBtn').on("click", confirmBtnfunc);
		//$(".btn.confirmBtn").attr("id",confirmBtnId)
	}

    if(type!='error')
        $('.msgBox').addClass('success');
    else
        $('.msgBox').removeClass('success');

    $('.msgBox').fadeIn();

    if (reload==true) {
        $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".msgBox").fadeOut();
            location.reload();
        }
        });
        $('.msgBox .exit , .msgBox .cancelBtn').click(function() {
            $('.msgBox').fadeOut();
            location.reload();
        });
        $('.msgBox').click(function(e) {
            if (e.target === this) {
                $('.msgBox').fadeOut();
                location.reload();
            };
        });
    } else if(reload==false) {
        $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".msgBox").fadeOut();
        }
        });
        $('.msgBox .exit , .msgBox .cancelBtn').click(function() {
            $('.msgBox').fadeOut();
        });
        $('.msgBox').click(function(e) {
            if (e.target === this) {
                $('.msgBox').fadeOut();
            };
        });
    } else {
        $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".msgBox").fadeOut();
            window.location.href = APP_URL+reload;
        }
        });
        $('.msgBox .exit , .msgBox .cancelBtn').click(function() {
            $('.msgBox').fadeOut();
            window.location.href = APP_URL+reload;
        });
        $('.msgBox').click(function(e) {
            if (e.target === this) {
                $('.msgBox').fadeOut();
                window.location.href = APP_URL+reload;
            };
        });
    }
}

function ShowLoader() {
    $('#loading').fadeIn();
}
function HideLoader() {
    $('#loading').fadeOut();
}

function CallAjax(ajaxurl,ajaxdata,pnlres) {
    $('#loading').fadeIn();
    $.ajax({
        method: "POST",
        url: ajaxurl,
        data: ajaxdata,
        statusCode: {
            404: function() {
                ShowMessage('error','خطا در ارسال اطلاعات.<br />لطفا اتصال اینترنت خود را بررسی کنید.','false','false');
            }
        }
        ,
        success:function(data) {
            $('#loading').fadeOut();
            if(pnlres!='')
                $('#'+pnlres).html(data)
        }
    });
}

function CallAjaxFunc(ajaxurl,ajaxdata,func) {
    $.ajax({
        method: "POST",
        url: ajaxurl,
        data: ajaxdata,
        statusCode: {
            404: function() {
                ShowMessage('error','خطا در ارسال اطلاعات.<br />لطفا اتصال اینترنت خود را بررسی کنید.','false','false');
            }
        }
        ,
        success:func
    });
}

function ToPersianNumber(number){
    var str = number.toString();
    var ennum = ['0','1','2','3','4','5','6','7','8','9'];
    var fanum = ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'];
    for(var j = 0; j < str.length; j++) {
        for(var i = 0; i < fanum.length; i++){
            str = str.replace(ennum[i], fanum[i])
        }
    }

    return str;
}

function ToPersianPriceNumber(number){
    var str = number.toString();
    var ennum = ['0','1','2','3','4','5','6','7','8','9'];
	var fanum = ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'];
	if(!str.includes("."))
		str = str.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1،");
	else
		str = str.slice(0, 3);
    for(var j = 0; j < str.length; j++) {
        for(var i = 0; i < fanum.length; i++){
            str = str.replace(ennum[i], fanum[i])
        }
	}
    return str;
}

function ShowPopUp(title,url,parametr,width,height) {
	$('.modal_box .innerbox').css('max-width',width);
	$('.modal_box .innerbox').css('min-height',height);
	$(".main_text .title").html(title);
	$(".modal_box").show();
	CallAjax(url,parametr,"mohtava");
}

function ShowText(title,text) {
	$(".main_text .title").html(title);
	$("#mohtava").html(text);
	$(".modal_box").show();
}


</script>
</body>

</html>
