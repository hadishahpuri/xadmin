@push('headStyle')
<style>
    label.myLabel input[type="file"] {
        position:absolute;
        top: -1000px;
    }

    /***** Example custom styling *****/
    .myLabel {
        border: 2px solid #AAA;
        border-radius: 4px;
        padding: 2px 5px;
        margin: 2px;
        background: #DDD;
        display: inline-block;
    }
    .myLabel:hover {
        background: #CCC;
    }
    .myLabel:active {
        background: #CCF;
    }
    .myLabel :invalid + span {
        color: #A44;
    }
    .myLabel :valid + span {
        color: #4A4;
    }
</style>
@endpush
{{--<div class="row">--}}
{{--    <div>--}}
{{--        <img style="width: 300px; height: 300px;" src="{{ asset('img/profiles/' . Auth::user()->profile_hash_key . '.jpg') }}">--}}
{{--    </div>--}}
{{--</div>--}}
<div class="row" id="dialogSuccess">
    <div class="uploadPicture mainPic">
        <div class="inner">
            <form action="{{ route('UpdateProfileImage') }}" method="POST" id="UpldImgForm" name="UpldImgForm">
                <label for="active_icon" class="myLabel"></label>
                    <img src="" alt="" class="previewPic" style="width: 300px; height: 300px;">
                <input type="file" name="image" id="image" onchange="readURL(this);" accept="image/jpeg, image/png" />
                <button type="submit" class="btn btn-info">تعویض</button>
            </form>
        </div>
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).siblings('.previewPic').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        $('#UpldImgForm').ajaxForm({
            success: function (res) {
                console.log(res);
                $('#dialogSuccess').append(`
                    <div class="alert alert-success" style="width: 100%">با موفقیت تعویض شد</div>
                `)
            }
        });
    })

</script>
