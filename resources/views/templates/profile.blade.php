@extends('layouts.app')

@section('content')
@include('layouts.header2')
@endsection

@push('newscript')
    <script>
        $('#profile').click(function () {
            ShowPopUp("Profile", "{{url(route("EditProfileImage"))}}",{id:'id',readonly:true})
        })
    </script>
@endpush
