@extends('layouts.app')

@push('headStyle')
    <style>
        .backGround {
            background-image: url({{ asset( Auth::user()->profile_hash_key) }});
        }
    </style>
@endpush

@section('content')

        <!-- Header -->
        <div id="backGround" class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 600px; background-size: cover; background-position: center top;">
            <!-- Mask -->
            <span class="mask bg-gradient-default opacity-8"></span>
            <!-- Header container -->
            <div class="container-fluid d-flex align-items-center">
                <div class="row">
                    <div class="col-lg-7 col-md-10">
                        <h1 class="display-2 text-white">سلام {{ Auth::user()->name }}</h1>
{{--                        <a href="#!" class="btn btn-info">Edit profile</a>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                    <div class="card card-profile shadow">
                        <div class="row justify-content-center">
                            <div class="col-lg-3 order-lg-2">
                                <div class="card-profile-image">
                                    <a href="#">
                                        <img src="{{ asset( Auth::user()->profile_hash_key) }}" id="proImg" name="proImg" class="rounded-circle">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                        </div>
                        <div class="card-body pt-0 pt-md-4">
                            <div class="row">
                                <div class="col">
                                    <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <h3>
                                    {{ Auth::user()->name }}<span class="font-weight-light">, {{ Auth::user()->age }}</span>
                                </h3>
                                <div class="h5 font-weight-300">
                                    <i class="ni location_pin mr-2"></i>{{ Auth::user()->cellphone }}
                                </div>
                                <div class="h5 mt-4">
                                    <i class="ni business_briefcase-24 mr-2"></i>{{ Auth::user()->email }}
                                </div>

                                <hr class="my-4" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 order-xl-1">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">پروفایل ادمین</h3>
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('UpdateUser') }}" method="POST" id="frmUpdateUser" name="frmUpdateUser">
                                <h6 class="heading-small text-muted mb-4">اطلاعات فردی</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-username">نام</label>
                                                <input type="text" id="inptName" name="name" class="form-control form-control-alternative" placeholder="نام"
                                                       value="{{ Auth::user()->name }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-email">ایمیل</label>
                                                <input type="email" id="inptEmail" name="email" class="form-control form-control-alternative"
                                                       placeholder="jesse@example.com" value="{{ Auth::user()->email }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-first-name">موبایل</label>
                                                <input type="text" id="inptCell" name="cellphone" class="form-control form-control-alternative"
                                                       placeholder="0913*******" value="{{ Auth::user()->cellphone }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">رزومه</label>
                                                <textarea id="inptCv" name="cv" class="form-control form-control-alternative" rows="5">{{ Auth::user()->cv }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button id="profileSubmitBtn" onclick="UpdateProfile(this)" class="btn btn-info">ویرایش پروفایل</button>
                                <hr class="my-4" />
                            </form>
                        </div>
                    </div>

                    @include('layouts.footer')
                </div>

{{--            </div>--}}

@endsection

@push('newscript')
    <script>
        CKEDITOR.replace('cv');

        $('#proImg').click(function () {
            ShowPopUp("Profile", "{{url(route("EditProfileImage"))}}",{id:'id',readonly:true})
        })

        $(document).ready(function () {
            $('#backGround').addClass('backGround');

        });
        function UpdateProfile (obj) {
            console.log(obj)
            ShowLoader()
            $('#frmUpdateUser').ajaxForm(ProfileSubmitSuccess)
            // CallAjaxFunc('/profile/update', {name: $('#inptName'), email:$('#inptEmail'), cellphone:$('#inptCell'), cv:$('#inptCv')}, ProfileSubmitSuccess)
        }

        function ProfileSubmitSuccess(res) {
            HideLoader()
            console.log(res)
        }
    </script>
@endpush
