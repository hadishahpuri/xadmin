@extends('layouts.app')


@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6" id="orderCard">
                        <div class="card card-stats mb-4 mb-xl-0" id="newNewsForm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">ثبت سفارش جدید</h5>
                                        <span class="h2 font-weight-bold mb-0" id="customersNum"></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">سفارشات</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">مشتری</th>
                                <th scope="col">محصولات</th>
                                <th scope="col">مبلغ کل</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentOrderId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var orderIds = [];
        var offset = 1;
        var ajaxInProgress = false;

        if (first === true) {
            first = false;
            ShowLoader()
            CallAjaxFunc('/orders/data', {offset:offset}, OrdersTableGenerator)
        }

        function OrdersTableGenerator(res) {
            HideLoader()
            first = false
            var orders = res.orders;
            orders.forEach(order => {
                $('#tblBody').append(`
                 <tr data-field-order-id="${order.id}">
                            <td>
                                ${order.name + ' ' + order.last_name}
                            </td>
                            <td data-field-order-products="${order.id}">
                              <div class="avatar-group" data-field-avatars="${order.id}">
                              </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${order.price}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-order-status="${order.id}"></span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <h6>تغییر وضعیت به</h6>
                                        <a class="dropdown-item" href="#" data-field-id="${order.id}" onclick="ChangeOrderStatusToPayed(this)">پرداخت شد</a>
                                        <a class="dropdown-item" href="#" data-field-id="${order.id}" onclick="ChangeOrderStatusToDelivered(this)">تحویل داده شد</a>
                                        <a class="dropdown-item" href="#" data-field-id="${order.id}" onclick="ChangeOrderStatusToCanceled(this)">لغو شد</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)

                var avatarDiv = $('[data-field-avatars ="'+ order.id +'"]');

                order.info.forEach(item => {
                    avatarDiv.append(`
                        <a href="#" class="avatar avatar-sm" title="${item.title + ',' + ' price : ' + item.price + ',' + item.count}" data-toggle="tooltip">
                            <img src="${item.hash_1}" onerror="DefaultImg(this)" class="rounded-circle">
                        </a>
                    `)
                })
                $('[data-toggle ="tooltip"]').tooltip();

                if (order.new === 1) {
                    orderIds.push(order.id);
                }
                var orderStatus = $('[data-field-order-status ="'+ order.id +'"]');
                if (order.status === 'payed') {
                    orderStatus.text('پرداخت شد');
                }else if (order.status === 'delivered') {
                    orderStatus.text('تحویل داده شد');
                }else if (order.status === 'canceled') {
                    orderStatus.text('لغو شد');
                }
            })
            if (res.orders.length === 1) {
                $('#tblBody').children('tr').css("height", "100px")
            }
            offset = res.offset;
            first = false;

            if (orderIds.length > 0) {
                CallAjax('/orders/update_seen_status', {orders:orderIds})
            }
        }

        console.log(offset)
        $(function () {
            $(window).scroll(function () {

                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('/orders/data', {offset:offset}, OrdersTableGenerator)
                    }
                }

            })
        })


    })
    $('#orderCard').click(function () {
        location.href = '/orders/new/view';
    })


    function ChangeOrderStatusToDelivered(obj) {
        ShowLoader();
        CallAjaxFunc('/orders/change_status', {id:$(obj).attr('data-field-id'), status:"delivered"}, StatusToDeliveredSuccess)
    }

    function StatusToDeliveredSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var order = $('[data-field-order-status ="'+ res.order.id +'"]');
            order.text('تحویل داده شد');
        }
    }

    function ChangeOrderStatusToPayed(obj) {
        ShowLoader();
        CallAjaxFunc('/orders/change_status', {id:$(obj).attr('data-field-id'), status:"payed"}, StatusToPayedSuccess)
    }

    function StatusToPayedSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var order = $('[data-field-order-status ="'+ res.order.id +'"]');
            order.text('پرداخت شد');
        }
    }


    function ChangeOrderStatusToCanceled(obj) {
        ShowLoader();
        CallAjaxFunc('/orders/change_status', {id:$(obj).attr('data-field-id'), status:"canceled"}, StatusToCanceledSuccess)
    }

    function StatusToCanceledSuccess(res) {
        HideLoader();
        if (res.msg !== 'success') {
            ShowMessage('error', res.error, false, false);
        }else {
            ShowMessage('success', 'با موفقیت انجام شد', false, false);
            var order = $('[data-field-order-status ="'+ res.order.id +'"]');
            order.text('لغو شد');
        }
    }

</script>
@endpush
