@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>

<div class="card-body" id="customerPopUp">
{{--    <form action="{{ route('NewOrder') }}" method="POST" class="formblk" id="frmNewOrder" name="frmNewOrder">--}}
        <h6 class="heading-small text-muted mb-4U">ثبت سفارش جدید</h6>
        <div class="pl-lg-4">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">مشتری</label>
                        <select class="form-control form-control-alternative" name="customer_id" id="customer_id">
                            <option disabled selected>انتخاب کنید ...</option>
                            @foreach($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name . ' ' .  $customer->last_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">محصول</label>
                        <select class="form-control form-control-alternative" name="products" id="products">
                            <option disabled selected>انتخاب کنید ...</option>
                            @foreach($products as $product)
                                <option value="{{ $product->id }}" title="{{ $product->title }}" data-field-price="{{ $product->price }}">{{ $product->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">وضعیت</label>
                        <select class="form-control form-control-alternative" name="status" id="status">
                            <option disabled selected>انتخاب کنید ...</option>
                                <option value="none">هیچ کدام</option>
                                <option value="payed">پرداخت شد</option>
                                <option value="delivered">تحویل داده شد</option>
                                <option value="canceled">لغو شد</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <h3>محصولات افزوده شده به سفارش</h3>
                <div class="col-lg-12" id="addedProducts"></div>
            </div>
        </div>
        <div style="height: 20px;"></div>
        <button type="submit" class="btn btn-info" id="newOrderBTN">ثبت</button>
        <hr class="my-4" />
{{--    </form>--}}
</div>

    <!-- The Modal -->
    <div id="myModal" class="order-new-modal">
        <!-- Modal content -->
        <div class="order-new-modal-content">
            <span class="close">&times;</span>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">تعداد محصول</label>
                        <input type="number" id="count" name="count" class="form-control form-control-alternative">
                    </div>
                </div>
            </div>
            <button class="btn btn-info" id="addProductBtn">ثبت</button>
        </div>
    </div>


@include('layouts.footer')
@endsection
@push('newscript')
<script>
        var currentProduct = null;
        var title = null;
        var price = null;
        var  addedProductsArray = [];

        $('#products').change(function () {
            currentProduct = $(this).val()
            title = $(this).find("option:selected").attr('title')
            price = $(this).find("option:selected").attr('data-field-price')
            console.log(currentProduct)
            console.log(title)
            modal.style.display = "block";
            $('#count').focus()
        })
        // Get the modal
        var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            $('#products').val("")
            modal.style.display = "none";
        }

// When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                $('#products').val("")
                modal.style.display = "none";
            }
        }


        $('#addProductBtn').click(function () {
            if (! $('#count').val()) {
                alert('پر کردن قسمت تعداد الزامی است')
            }else {
                addedProductsArray.push({id:currentProduct, count:$('#count').val(), price: price * $('#count').val()})
                $('#addedProducts').append(`
                <div class="col-lg-3" data-field-added="${currentProduct}">
                    <div class="added-products">
                        <span class="close" data-field-added-span="${currentProduct}" onclick="RemoveAddedProduct(this)">&times;</span>
                        <div class="added-product-content" title="${title}">${title + ' count: ' + $('#count').val()}</div>
                    </div>
                </div>
                `)
                $('#products').val("")
                $('#count').val("");
                modal.style.display = "none";
                console.log(addedProductsArray)
            }
        })


        function RemoveAddedProduct (obj) {
            var addedProduct = $('[data-field-added ="'+ $(obj).attr("data-field-added-span") +'"]');
            console.log(addedProduct)
            var removeOjb = {id: $(obj).attr("data-field-added-span"), title: $(obj).next().attr("title")}
            console.log(removeOjb)
            var newArray = addedProductsArray.filter(function (item) {
                return item.id !== removeOjb.id
            })
            addedProductsArray = newArray;
            console.log(newArray)
            addedProduct.empty()
        }



        $('#newOrderBTN').click(function () {
            ShowLoader()
            CallAjaxFunc('/orders/new', {customer_id:$('#customer_id').val(), products:addedProductsArray, status:$('#status').val()}, NewOrderSuccess)
        })

    function NewOrderSuccess(res) {
        HideLoader();
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت ثبت شد', false, false);
        }else {
            ShowMessage('error', res.error, false, false);
        }
    }


</script>
@endpush
