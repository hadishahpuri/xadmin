<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="/">
        <img src="{{ asset('img/brand/new-grunge-hexagon-frame-2.png') }}" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="{{ Auth::user()->profile_hash_key }}">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">خوش آمدید!</h6>
            </div>
            <a href="{{ route('UserProfile') }}" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>پروفایل ادمین</span>
            </a>
            <a href="{{ route('SettingsIndex') }}" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>تنظیمات سایت</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="{{ route('LogOut') }}" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>خروج</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="#">
                <img src="{{ asset('img/brand/blue.png') }}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="جستجو" aria-label="جستجو">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form>
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
          <a class=" nav-link" id="dash" href="{{ route('ShowHomePage') }}"> <i class="ni ni-tv-2 text-primary"></i> پنل مدیریت
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="cust" href="{{ route('CustomerIndex') }}">
              <i class="ni ni-single-02 text-yellow"></i> مشتری ها
            </a>
          </li>
            <li class="nav-item">
                <a class="nav-link " id="prod" href="{{ route('ProductIndex') }}">
                    <i class="fas fa-box-open" style="color: #21af27;"></i> محصولات
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="cats" href="{{ route('CatsIndex') }}">
                    <i class="far fa-clone"></i> دسته های محصولات
                </a>
            </li>
          <li class="nav-item">
            <a class="nav-link " id="news" href="{{ route('NewsIndex') }}">
                <i class="ni ni-planet text-blue"></i> اخبار
            </a>
          </li>
{{--          <li class="nav-item">--}}
{{--            <a class="nav-link " href="">--}}
{{--              <i class="ni ni-bullet-list-67 text-red"></i> Videos--}}
{{--            </a>--}}
{{--          </li>--}}
          <li class="nav-item">
            <a class="nav-link" id="orde" href="{{ route('OrdersIndex') }}">
                <i class="ni ni-delivery-fast text-indigo"></i> سفارشات
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="tran" href="{{ route('TransactionIndex') }}">
                <i class="fas fa-exchange-alt"></i> تراکنش ها
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="tran" href="{{ route('MessagesIndex') }}">
                <i class="fas fa-envelope" style="color: #6a6acf"></i> پیام ها
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="tran" href="{{ route('SettingsIndex') }}">
                <i class="fas fa-cog" style="color: red;"></i> تنظیمات
            </a>
          </li>
        </ul>
        <!-- Divider -->
        <hr class="my-3">
      </div>
    </div>
  </nav>
