<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    {{ $page->title ?? 'X ADMIN' }}
  </title>
    <meta name="description" content="{{ $page->description ?? 'X Admin Professional Dashboard'}}">
  <!-- Favicon -->
  <link href="{{ asset('img/brand/favicon.png') }}" rel="icon" type="image/png">
  <link href="{{ asset('js/nucleo/css/nucleo.css') }}" rel="stylesheet" />
  <link href="{{ asset('js/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{ asset('css/argon-dashboard.css') }}" rel="stylesheet" />
  <link href="{{ asset('js/chart.js/dist/Chart.min.css') }}" rel="stylesheet" />

    @stack('headStyle')

</head>
<body class="">

    @include('layouts.sidebar')
<div class="main-content">
    @include('layouts.navbar')
    @yield('content')
</div>

    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
            </div>
        </div>
    </div>
    <div class="modal_box">
        <div class="innerbox">
            <div class="exit"></div>
            <div class="main_text">
                <div class="title">تیتر پاپ اپ</div>
                <div class="mohtava" id="mohtava">
                    <div id="box_loading">
                        <div id="loading-center">
                            <div id="loading-center-absolute">
                                <div class="box_object" id="box_object_one"></div>
                                <div class="box_object" id="box_object_two"></div>
                                <div class="box_object" id="box_object_three"></div>
                                <div class="box_object" id="box_object_four"></div>
                                <div class="box_object" id="box_object_five"></div>
                                <div class="box_object" id="box_object_six"></div>
                                <div class="box_object" id="box_object_seven"></div>
                                <div class="box_object" id="box_object_eight"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="msgBox">
        <div class="innerbox">
            <div class="exit"></div>
            <div class="topSide"></div>
            <div class="bottomSide"></div>
            <div class="btns">
                <div class="btn confirmBtn">تایید</div>
                <div class="btn cancelBtn">لغو</div>
            </div>
        </div>
    </div>

    <!--   Core   -->
    <script src="{{ asset('js/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery/jquery.form.js') }}"></script>
    <script src="{{ asset('js/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!--   Optional JS   -->
    <script src="{{ asset('js/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('js/chart.js/dist/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('js/chart.js/dist/Chart.extension.js') }}"></script>

    <script src="{{ asset('js/jQuery-Plugin-for-Client-Side-Image-Resizing-canvasResize/canvasResize.js') }}"></script>
    <script src="{{ asset('js/jQuery-Plugin-for-Client-Side-Image-Resizing-canvasResize/jquery.canvasResize.js') }}"></script>
    <!--   Argon JS   -->
    <script src="{{ asset('js/argon-dashboard.js') }}"></script>
    <script src="{{ asset('js/ckeditor_4.14.1_99fb88fab953/ckeditor/ckeditor.js') }}"></script>
    @stack('newscript')
    <script>
    @stack('script')

        $('.modal_box').height($(window).outerHeight(true));


    $('#dash').click(function () {
        $('#dash').addClass('active');
        $('#cust').removeClass('active');
        $('#prod').removeClass('active');
        $('#news').removeClass('active');
        $('#orde').removeClass('active');
        $('#tran').removeClass('active');
    });
    $('#cust').click(function () {
        $('#cust').addClass('active');
        $('#dash').removeClass('active');
        $('#prod').removeClass('active');
        $('#news').removeClass('active');
        $('#orde').removeClass('active');
        $('#tran').removeClass('active');
    });
    $('#prod').click(function () {
        $('#prod').addClass('active');
        $('#cust').removeClass('active');
        $('#dash').removeClass('active');
        $('#news').removeClass('active');
        $('#orde').removeClass('active');
        $('#tran').removeClass('active');
    });
    $('#news').click(function () {
        $('#news').addClass('active');
        $('#cust').removeClass('active');
        $('#prod').removeClass('active');
        $('#dash').removeClass('active');
        $('#orde').removeClass('active');
        $('#tran').removeClass('active');
    });
    $('#orde').click(function () {
        $('#orde').addClass('active');
        $('#cust').removeClass('active');
        $('#prod').removeClass('active');
        $('#news').removeClass('active');
        $('#dash').removeClass('active');
        $('#tran').removeClass('active');
    });
    $('#tran').click(function () {
        $('#tran').addClass('active');
        $('#cust').removeClass('active');
        $('#prod').removeClass('active');
        $('#news').removeClass('active');
        $('#orde').removeClass('active');
        $('#dash').removeClass('active');
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>



</body>

</html>
