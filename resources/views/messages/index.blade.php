@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6" id="customersCard">
                        <div class="card card-stats mb-4 mb-xl-0" id="newCustomerForm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">فیلتر کردن پیام ها <i class="fas fa-envelope" style="color: #6a6acf"></i></label>
                                        <select class="form-control form-control-alternative" id="messageFilters">
                                            <option value="all">همه</option>
                                            <option value="message">فقط پیام ها </option>
                                            <option value="comment">فقط کامنت ها</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row mt-5" id="MT5">
            <div class="col">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-white mb-0">پیام ها</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">فرستنده</th>
                                <th scope="col">محتوا</th>
                                <th scope="col">نوع پیام</th>
                                <th scope="col">نوع ارسال</th>
                                <th scope="col">مربوط به</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="messageTblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataLoad"></div>
    <input type="hidden" id="currentMessageId">
    @include('layouts.footer')
@endsection

@push('newscript')
<script>
    $(document).ready(function () {

        var docHeight = $(document).height();
        var first = true;
        var filter = "all";
        var offset = 1;
        var ajaxInProgress = false;
        var messageIds = [];

        if (first === true) {
            first = false;
            CallAjaxFunc('/messages/data', {offset:offset, filter:filter}, MessagesTableGenerator)
        }


        $('#messageFilters').change(function () {
            console.log($(this).val());
            $('#messageTblBody').empty();
            filter = $(this).val();
            offset = 1;
            ShowLoader()
            CallAjaxFunc('messages/data', {offset:offset, filter:filter}, MessagesTableGenerator)
        })


        function MessagesTableGenerator(res) {
            console.log(res)
            HideLoader()
            first = false
            messageIds = [];
            var messages = res.messages;
            messages.forEach(message => {
                $('#messageTblBody').append(`
                 <tr data-field-message-id="${message.id}">
                            <td>
                                ${message.signature}
                            </td>
                            <td style="white-space: normal; overflow: hidden; max-width: 100px;">
                              <span class="badge badge-dot mr-4">
                                ${message.content}
                              </span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-message-message-type="${message.id}"></span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-message-type="${message.id}"></span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-p-title="${message.id}"></span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center" id="rowStatus">
                                    <span class="mr-2" data-field-message-status="${message.id}"></span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" data-field-message-dropdown="${message.id}">
                                        <a class="dropdown-item" href="#" data-field-id="${message.id}" onclick="ActiveMessageClickChecking(this)">فعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${message.id}" onclick="DeActiveMessageClickChecking(this)">غیر فعال کردن</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
            `)

                if (message.message_type === 'comment') {
                    $('[data-field-message-dropdown ="'+ message.id +'"]').append(`
                        <a class="dropdown-item" href="#" data-field-id="${message.id}" data-field-edit-message="${message.id}" onclick="ReplyMessage(this)">ریپلای</a>
                    `)
                }

                var messageMessageType = $('[data-field-message-message-type ="'+ message.id +'"]');
                if (message.message_type === 'message') {
                    messageMessageType.text('پیام')
                }else if (message.message_type === 'comment') {
                    messageMessageType.text('کامنت')
                }else if (message.message_type === 'complain') {
                    messageMessageType.text('شکایت')
                }

                var messageType = $('[data-field-message-type ="'+ message.id +'"]');
                if (message.type === 'normal') {
                    messageType.text('معمولی')
                }else if (message.type === 'reply') {
                    messageType.text('ریپلای')
                }

                var messageStatus = $('[data-field-message-status ="'+ message.id +'"]');
                if (message.row_status === 'active') {
                    messageStatus.text('فعال')
                }else if (message.row_status === 'suspended') {
                    messageStatus.text('غیر فعال')
                }

                var pTitleMessage = $('[data-field-p-title ="'+ message.id +'"]');
                console.log(pTitleMessage)
                if (message.p_title !== null) {
                    pTitleMessage.text(message.p_title);
                }else {
                    pTitleMessage.text('هیچ کدام');
                }
                ajaxInProgress = false;
                if (message.new === 1) {
                    messageIds.push(message.id);
                }

                if (message.type === 'reply') {
                    var messageRow = $('[data-field-message-id ="'+ message.id +'"]');
                    messageRow.css('background-color', '#0f6674')
                }

            })


            if (res.messages.length === 1) {
                $('#tblBody').children('tr').css("height", "120px")
            }

            offset = res.offset;
            if (messageIds.length > 0) {
                CallAjax('messages/update_status', {messages:messageIds})
            }
        }

        console.log(offset);

        $(function () {
            $(window).scroll(function () {
                var top_of_element = $("#dataLoad").offset().top;
                var bottom_of_element = $("#dataLoad").offset().top + $("#dataLoad").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                    if (ajaxInProgress === false) {
                        ajaxInProgress = true;
                        CallAjaxFunc('messages/data', {offset:offset, filter:filter}, MessagesTableGenerator)
                    }
                }

            })
        })

    })



    function ActiveMessageClickChecking(obj) {
        $('#currentMessageId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,ActiveClickConfirm);
    }

    function ActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('messages/activate', {id:$('#currentMessageId').val(), type:"active"}, ActiveSuccess)
    }

    function ActiveSuccess(res) {
        HideLoader()
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت فعال شد', false, false);
            var message = $('[data-field-message-status ="'+ res.message.id +'"]');
            message.text('فعال');
        }
    }




    function DeActiveMessageClickChecking(obj) {
        $('#currentMessageId').val($(obj).attr('data-field-id'))
        ShowMessage("error","آیا مطمئن هستید؟",true,false,DeActiveClickConfirm);
    }

    function DeActiveClickConfirm() {
        ShowLoader();
        CallAjaxFunc('/messages/activate', {id:$('#currentMessageId').val(), type:"deActive"}, DeActiveSuccess)
    }

    function DeActiveSuccess(res) {
        HideLoader()
        if (res.msg === 'success') {
            ShowMessage('success', 'با موفقیت غیرفعال شد', false, false);
            var message = $('[data-field-message-status ="'+ res.message.id +'"]');
            message.text('غیر فعال');
        }
    }


    function ReplyMessage(obj) {
        var id = $(obj).attr('data-field-id');
        ShowPopUp("ریپلای پیام", "{{url(route("ReplyMessageView"))}}",{id:id,readonly:true})
    }
</script>
@endpush
