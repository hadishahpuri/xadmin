<div class="card-body" id="NewMessagePopUp">
    <form action="{{ route('NewMessage') }}" method="POST" id="frmNewMessage" name="frmNewMessage">
        <h6 class="heading-small text-muted mb-4U">Reply Message</h6>
        <div class="pl-lg-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">Replied Message</label>
                        <textarea type="text" name="repliedMessage" style="color: black" disabled class="form-control form-control-alternative">{{ $message->content }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">Content</label>
                        <textarea type="text" name="text" class="form-control form-control-alternative"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="reply_id" id="reply_id" value="{{ $message->id }}">
        <input type="hidden" name="p_title" id="p_title" value="{{ $message->p_title }}">
        <input type="hidden" name="product_id" id="product_id" value="{{ $message->product_id }}">
        <button type="submit" class="btn btn-info" id="NewMessageBtn">Send</button>
        <hr class="my-4" />
    </form>
</div>

<script>
    if (screen.width < 900) {
        console.log('small screen');
        $('#NewMessagePopUp').addClass('margin_top', '50px').addClass('margin_bottom', '50px');
    }
    $('#NewMessageBtn').click(function () {
        ShowLoader()
        $('#frmNewMessage').ajaxForm(function (res) {
            HideLoader();
            if (res.msg === 'success') {
                ShowMessage('success', 'successfully sent', false, false);
                PrependNewMessage(res.message);
            }
        });
    })

    function PrependNewMessage(message) {
        $('#messageTblBody').prepend(`
                 <tr data-field-message-id="${message.id}" style="margin-bottom: 10px; background-color: #0f6674">
                            <td>
                                ${message.signature}
                            </td>
                            <td style="white-space: normal; overflow: hidden">
                              <span class="badge badge-dot mr-4">
                                ${message.content}
                              </span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${message.message_type}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2">${message.type}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <span class="mr-2" data-field-p-title="${message.id}">${message.p_title}</span>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center" id="rowStatus">
                                    <span class="mr-2" id="rowStatusSpan">${message.row_status}</span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#" data-field-id="${message.id}" onclick="ActiveMessageClickChecking(this)">active</a>
                                        <a class="dropdown-item" href="#" data-field-id="${message.id}" onclick="DeActiveMessageClickChecking(this)">deActive</a>
                                        <a class="dropdown-item" href="#" data-field-id="${message.id}" onclick="ReplyMessage(this)">reply</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
    `)
    }
</script>
