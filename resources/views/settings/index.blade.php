@extends('layouts.app')

@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">تنظیمات صفحات سایت</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('UpdatePages') }}" method="POST" id="frmUpdateContact" name="frmUpdateContact">
                            <h6 class="heading-small text-muted mb-4">{{ $contact->title }} صفحه</h6>
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-username">عنوان</label>
                                            <input type="text" name="title" class="form-control form-control-alternative"
                                                   value="{{ $contact->title }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">توظیحات</label>
                                            <input type="text" name="description" class="form-control form-control-alternative"
                                                   value="{{ $contact->description }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">متن داخل صفحه</label>
                                            <textarea type="text" name="text" id="contactText" class="form-control form-control-alternative">{!! $contact->text !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" id="id" value="{{ $contact->id }}">
                            <button type="submit" class="btn btn-info" id="UpdateContactBtn">بروزرسانی</button>
                            <hr class="my-4" />
                        </form>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('UpdatePages') }}" method="POST" id="frmUpdateAbout" name="frmUpdateAbout">
                            <h6 class="heading-small text-muted mb-4">{{ $about->title }} صفحه</h6>
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-username">عنوان</label>
                                            <input type="text" name="title" class="form-control form-control-alternative"
                                                   value="{{ $about->title }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">توضیحات</label>
                                            <input type="text" name="description" class="form-control form-control-alternative"
                                                   value="{{ $about->description }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">متن داخل صفحه</label>
                                            <textarea type="text" name="text" id="aboutText" class="form-control form-control-alternative">{!! $about->text !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" id="id" value="{{ $about->id }}">
                            <button type="submit" class="btn btn-info" id="AboutUsUpdateBtn">بروزرسانی</button>
                            <hr class="my-4" />
                        </form>
                    </div>
                </div>

                @include('layouts.footer')
            </div>
@endsection

@push('newscript')
    <script>
        $(document).ready(function () {
            var aboutText = document.getElementById('aboutText')
            var contactText = document.getElementById('contactText')
            CKEDITOR.replace(aboutText);
            CKEDITOR.replace(contactText);


            $('#UpdateContactBtn').click(function () {
                ShowLoader()
                $('#frmUpdateContact').ajaxForm(function (res) {
                    HideLoader()
                    if (res.msg === 'success') {
                        ShowMessage('success', 'successfully updated', false, false)
                    }else {
                        ShowMessage('error', res.error, false, false)
                    }
                })
            })

            $('#AboutUsUpdateBtn').click(function () {
                ShowLoader()
                $('#frmUpdateAbout').ajaxForm(function (res) {
                    HideLoader()
                    if (res.msg === 'success') {
                        ShowMessage('success', 'successfully updated', false, false)
                    }else {
                        ShowMessage('error', res.error, false, false)
                    }
                })
            })
        })
    </script>
@endpush
