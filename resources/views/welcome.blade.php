@extends('layouts.app')
@section('content')

@include('layouts.header')
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card bg-gradient-default shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-light ls-1 mb-1">بررسی</h6>
                            <h2 class="text-white mb-0">بازدید سایت</h2>
                        </div>
                        <div class="col">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Chart -->
                    <div class="chart">
                        <!-- Chart wrapper -->
                        <canvas id="chart-sales" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="row mt-5">
        <div class="col-xl-8 mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">اخبار</h3>
                </div>
                <div class="col text-right">
                  <a href="{{ route('NewsIndex') }}" id="buttonSize" class="btn btn-sm btn-primary">همه</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">عنوان</th>
                    <th scope="col">تعداد بازدید</th>
                    <th scope="col">تعداد لایک</th>
                    <th scope="col">وضعیت فعلی</th>
{{--                    <th scope="col">Actions</th>--}}
                  </tr>
                </thead>
                <tbody id="tblNews">
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">پرفروش ترین محصولات</h3>
                </div>
                <div class="col text-right">
                  <a href="{{ route('ProductIndex') }}" class="btn btn-sm btn-primary">همه</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">عنوان</th>
                    <th scope="col">تعداد سفارش</th>
                  </tr>
                </thead>
                <tbody id="tblOrders">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    <input type="hidden" name="currentNewsId" id="currentNewsId">
      <!-- Footer -->
        @include('layouts.footer')

</div>

@endsection

@push('newscript')
    <script>
    $(document).ready(function () {
        console.log($(screen.height));
        console.log($(screen.width));


        var visits = $.ajax({
            url : '/get_visits',
            method : 'POST',
            data : {'user':'user'}
        }).done(function (res) {
            console.log(res);
            var chartLables;

            if (screen.width < 600) {
                chartLables = [res[0].month_num, res[1].month_num, res[2].month_num, res[3].month_num, res[4].month_num, res[5].month_num,
                            res[6].month_num, res[7].month_num, res[8].month_num, res[9].month_num, res[10].month_num, res[11].month_num]
            }else {
                chartLables = [res[0].month, res[1].month, res[2].month, res[3].month, res[4].month, res[5].month,
                            res[6].month, res[7].month, res[8].month, res[9].month, res[10].month, res[11].month]
            }
            //
            // Charts
            //

            'use strict';

            //
            // Sales chart
            //

            var SalesChart = (function() {

                // Variables

                var $chart = $('#chart-sales');

                // Methods

                function init($chart) {

                    var salesChart = new Chart($chart, {
                        type: 'line',
                        options: {
                            scales: {
                                yAxes: [{
                                    gridLines: {
                                        lineWidth: 1,
                                        color: Charts.colors.gray[900],
                                        zeroLineColor: Charts.colors.gray[900]
                                    },
                                    ticks: {
                                        callback: function(value) {
                                            if (!(value % 10)) {
                                                return value;
                                            }
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(item, data) {
                                        var label = data.datasets[item.datasetIndex].label || '';
                                        var yLabel = item.yLabel;
                                        var content = '';

                                        if (data.datasets.length > 1) {
                                            content += '<span class="popover-body-label mr-auto">' + label + '</span>';
                                        }

                                        content += '<span class="popover-body-value">' + yLabel + '</span>';
                                        return content;
                                    }
                                }
                            }
                        },
                        //, 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                        data: {
                            labels: chartLables,
                            datasets: [{
                                label: 'Performance',
                                data:[res[0].count, res[1].count, res[2].count, res[3].count, res[4].count, res[5].count,
                                    res[6].count, res[7].count, res[8].count, res[9].count, res[10].count, res[11].count]
                            }]
                        }
                    });

                    // Save to jQuery object

                    $chart.data('chart', salesChart);

                };


                // Events

                if ($chart.length) {
                    init($chart);
                }

            })();


        });


        $.ajax({
            url: '{{ route('HomePageData') }}',
            method: 'GET'
        }).done(function (res) {
            console.log(res);
            $('#customersNum').text(res.customers.length);
            $('#transactionsNum').text(res.transactions.length);
            $('#ordersNum').text(res.orders.length);
            $('#messagesNum').text(res.messages.length);
            MostOrdersGenerator(res.products);
            NewsTblGenerator(res.news);
        });

        function NewsTblGenerator(news) {
            news.forEach(item =>{
                $('#tblNews').append(`
                    <tr data-field-id="${item.id}">
                        <th scope="row">
                          ${item.title}
                        </th>
                        <td>
                          ${item.view_count}
                        </td>
                        <td>
                          ${item.like_count}
                        </td>
                        <td data-news-row-status="${item.id}">
                        </td>
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#" data-field-id="${item.id}" onclick="ActivateNewsClickChecking(this)">فعال کردن</a>
                                        <a class="dropdown-item" href="#" data-field-id="${item.id}" onclick="deActivateNewsClickChecking(this)">غیرفعال کردن</a>
                                        <a class="dropdown-item" href="/news/edit/view/${item.id}" data-field-id="${item.id}">ویرایش</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                `)

                var news = $('[data-news-row-status ="'+ item.id +'"]');
                if (item.row_status === 'active') {
                    news.text('فعال')
                }else if (item.row_status === 'suspended') {
                    news.text('غیرفعال')
                }
            })
        }



        $('#customersCard').click(function () {
            location.href = '/customers';
        })
        $('#transactionsCard').click(function () {
            location.href = '/transactions';
        })
        $('#ordersCard').click(function () {
            location.href = '/orders';
        })
        $('#messagesCard').click(function () {
            location.href = '/messages';
        })

        function MostOrdersGenerator(products) {
            products.forEach(product => {
                $('#tblOrders').append(`
                      <tr>
                        <th scope="row" style="overflow: hidden; white-space: normal;">
                          ${product.title}
                        </th>
                        <td>
                          ${product.orders_count}
                        </td>
                     </tr>
                `)
            })
        }
    });
    </script>
@endpush
