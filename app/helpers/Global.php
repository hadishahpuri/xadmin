<?php
use Illuminate\Support\Facades\DB;
function MiliTime()
{
    return round(microtime(true) * 1000);
}

function UpdateNotification($_refrence_id, $_user_id)
{
    DB::update("UPDATE `x_user_conversations` SET `notification` = `notification` + 1 WHERE `refrence_id` = $_refrence_id AND `owner_id` != $_user_id");
}

function RandomTextGenerator($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function IdentityFormGenerator()
{ 
    $identityForm = [
                    'formType' => 'IDENTITY',
                    'title' => [
                    'key' => 'identity',
                    'hint' => 'identity',
                    'viewType' => 'TEXT',
                    'regx' => null,
                    'text' => 'IDENTITY FORM',
                    'nullable' => false,
                    'width' => -1,
                    'height' => -2,
                    'margin' => [0,20,0,20],
                    'padding' => [0,0,0,0],
                    'gravityCenter' => true,
                    'isEnable' => true,
                    'backgroundColor' => null,
                    'inputNum' => false,
                    'textSize' => 16,
                    'textColor' => '#d10b0b',
                    'visibility' => 1,
                    'counterEnabled' => false,
                    'counterMaxLength' => 10,
                    ],
                    'elements' => array([
                        'key' => 'name',
                        'hint' => 'name',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => 30,
                        'counterEnabled' => true,
                        'counterMaxLength' => 30
                    ],[
                        'key' => 'last_name',
                        'hint' => 'last name',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => null,
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => 30,
                        'counterEnabled' => true,
                        'counterMaxLength' => 30
                    ],[
                        'key' => 'known_name',
                        'hint' => 'known name',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => true,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => 30,
                        'counterEnabled' => true,
                        'counterMaxLength' => 30
                    ],[
                        'key' => 'national_code',
                        'hint' => 'national code',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 20,
                        'counterEnabled' => true,
                        'counterMaxLength' => 20
                    ],[
                        'key' => 'birth_day',
                        'hint' => 'birth day',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => 20,
                        'counterEnabled' => true,
                        'counterMaxLength' => 20
                    ],[
                        'key' => 'gender',
                        'hint' => 'gender',
                        'viewType' => 'GENDER',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => 'gender:',
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [10,0,10,10],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => true,
                        'textSize' => 16,
                        'textColor' => null,
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => null,
                        'counterEnabled' => false,
                        'counterMaxLength' => null
                    ])
                ];  

    return ['result' => true, 'identity_form' => $identityForm];
    
}

function ContactFormGenerator()
{ 
    $contactForm = [
                    'formType' => 'CONTACT',
                    'title' => [
                    'key' => 'contact',
                    'hint' => 'contact',
                    'viewType' => 'TEXT',
                    'regx' => null,
                    'text' => 'CONTACT FORM',
                    'nullable' => false,
                    'width' => -1,
                    'height' => -2,
                    'margin' => [0,20,0,20],
                    'padding' => [0,0,0,0],
                    'gravityCenter' => true,
                    'isEnable' => true,
                    'backgroundColor' => null,
                    'inputNum' => false,
                    'textSize' => 16,
                    'textColor' => '#d10b0b',
                    'visibility' => 1,
                    'counterEnabled' => false,
                    'counterMaxLength' => 10,
                    ],
                    'elements' => array([
                        'key' => 'cellphone',
                        'hint' => 'cellphone',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 12,
                        'counterEnabled' => true,
                        'counterMaxLength' => 12
                    ],[
                        'key' => 'area_code',
                        'hint' => 'area code',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => null,
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 5,
                        'counterEnabled' => true,
                        'counterMaxLength' => 4
                    ],[
                        'key' => 'phone_number',
                        'hint' => 'phone number',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => true,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 10,
                        'counterEnabled' => true,
                        'counterMaxLength' => 10
                    ],[
                        'key' => 'email',
                        'hint' => 'email',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => 40,
                        'counterEnabled' => true,
                        'counterMaxLength' => 40
                    ],[
                        'key' => 'fax',
                        'hint' => 'fax',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 20,
                        'counterEnabled' => true,
                        'counterMaxLength' => 20
                    ])
                ];  

    return ['result' => true, 'contact_form' => $contactForm];
    
}
 

function LocationFormGenerator()
{ 
    $locationForm = [
                    'formType' => 'LOCATION',
                    'title' => [
                    'key' => 'location',
                    'hint' => 'location',
                    'viewType' => 'location',
                    'regx' => null,
                    'text' => 'LOCATION FORM',
                    'nullable' => false,
                    'width' => -1,
                    'height' => -2,
                    'margin' => [0,20,0,20],
                    'padding' => [0,0,0,0],
                    'gravityCenter' => true,
                    'isEnable' => true,
                    'backgroundColor' => null,
                    'inputNum' => false,
                    'textSize' => 16,
                    'textColor' => '#d10b0b',
                    'visibility' => 1,
                    'counterEnabled' => false,
                    'counterMaxLength' => 10,
                    ],
                    'elements' => array([
                        'key' => 'cellphone',
                        'hint' => 'cellphone',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 12,
                        'counterEnabled' => true,
                        'counterMaxLength' => 12
                    ],[
                        'key' => 'area_code',
                        'hint' => 'area code',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => null,
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 5,
                        'counterEnabled' => true,
                        'counterMaxLength' => 4
                    ],[
                        'key' => 'phone_number',
                        'hint' => 'phone number',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => true,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 10,
                        'counterEnabled' => true,
                        'counterMaxLength' => 10
                    ],[
                        'key' => 'email',
                        'hint' => 'email',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => false,
                        'maxLength' => 40,
                        'counterEnabled' => true,
                        'counterMaxLength' => 40
                    ],[
                        'key' => 'fax',
                        'hint' => 'fax',
                        'viewType' => 'TEXTINPUT',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => null,
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [0,0,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'gravityCenter' => false,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'inputNum' => true,
                        'maxLength' => 20,
                        'counterEnabled' => true,
                        'counterMaxLength' => 20
                    ])
                ];  

    return ['result' => true, 'location_form' => $locationForm];
    
}


function ImageFormGenerator()
{
    $imageForm = [
                'formType' => 'IMAGE',
                'title' => [
                'key' => 'image',
                'hint' => 'image',
                'viewType' => 'TEXT',
                'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                'text' => 'CHOOSE AN IMAGE',
                'nullable' => false,
                'width' => -1,
                'height' => -2,
                'margin' => [20,20,0,0],
                'padding' => [0,0,0,0],
                'gravityCenter' => true,
                'isEnable' => true,
                'backgroundColor' => null,
                'textSize' => 16,
                'textColor' => '#d10b0b',
                'visibility' => 1,
                'counterEnabled' => false,
                'counterMaxLength' => 10,
                ],
                'elements' => array(
                    [
                        'key' => 'name',
                        'hint' => 'image',
                        'viewType' => 'IMAGE',
                        'regx' => '^(([+]{1}[0-9]{2}|0)[0-9]{9})$',
                        'text' => "I don't have any image",
                        'nullable' => false,
                        'width' => -1,
                        'height' => -2,
                        'margin' => [20,20,0,0],
                        'padding' => [0,0,0,0],
                        'isEnable' => true,
                        'backgroundColor' => null,
                        'textSize' => 16,
                        'textColor' => '#2b84e2',
                        'visibility' => 1,
                        'counterEnabled' => false,
                        'counterMaxLength' => 10
                    ]
                )

            ];

    return ['result' => true, 'image_form' => $imageForm];
}
