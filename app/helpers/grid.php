<?php
/**
 * Created by PhpStorm.
 * User: dad
 * Date: 9/4/2017
 * Time: 9:52 AM
 */
use Maatwebsite\Excel\Facades\Excel;

use Morilog\Jalali;
use Morilog\Jalali\Jalalian;

function DrawSerachPannel($grid){
    ?>
    <div class="row">
        <div class="partition-1">
            <div class="search_box">
                <form id="<?=$grid["name"]?>Form" name="<?=$grid["name"]?>Form" action="<?=url(route($grid["route"]))?>" method="post" class="formblk">
                    <?php
                        if(isset($grid["search"])){
                            foreach ($grid["search"] as $key =>$value){
                                switch ($value){
                                    case "equal":
                                        ?>
                                            <div class="form-group">
                                                <label for="name"><?=$grid["columns"][$key]?></label>
                                                <?php
                                                    if ((isset($grid["formats"]))&&(array_key_exists($key,$grid["formats"]))){

                                                        switch ($grid["formats"][$key]){
                                                            case "Money":
                                                                ?><input type="text" name="inpt<?=$key?>" id="inpt<?=$key?>"><?php
                                                                break;

                                                            case "PersianDate":
                                                                ?><input type="text" name="inpt<?=$key?>" id="inpt<?=$key?>" onfocus="displayDatePicker(this.id);"><?php
                                                                break;

                                                            case "Date":
                                                                ?><input type="text" name="inpt<?=$key?>" id="inpt<?=$key?>" onfocus="displayDatePicker(this.id);"><?php
                                                                break;

                                                            case "List":
                                                                $default="all";
                                                                if(isset($grid["defaults"][$key]))
                                                                    $default=$grid["defaults"][$key];

                                                                DrawCombo($grid["lists"][$key],$default,"inpt".$key,true);
                                                                break;

                                                            case "Combo":
                                                                $default="all";
                                                                if(isset($grid["defaults"][$key]))
                                                                    $default=$grid["defaults"][$key];

                                                                DrawCombo($grid["combo"][$key],$default,"inpt".$key,true);
                                                                break;

                                                        }
                                                    }else{
                                                        ?>
                                                        <input type="text" name="inpt<?=$key?>" id="inpt<?=$key?>" />
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        <?php
                                        break;
                                    case "extra":
                                        ?>
                                        <div class="form-group">
                                                <label for="name"><?=$grid["extra"][$key]["title"]?></label>
                                        <?php
                                        $default="all";
                                        if(isset($grid["defaults"][$key]))
                                            $default=$grid["defaults"][$key];
                                        //dd($grid["extra"][$key]["data"]);
                                        $haveall=true;
                                        if(isset($grid["extra"][$key]["haveall"]))
                                            $haveall=$grid["extra"][$key]["haveall"];
                                        DrawCombo($grid["extra"][$key]["data"],$default,"inpt".$key,$haveall);
                                        ?>
                                        </div>
                                        <?php
                                        break;
                                    case "between":
                                        ?>
                                            <div class="form-group">
                                                <label for="sbuydate">از <?=$grid["columns"][$key]?></label>
                                                <?php
                                                    if ((isset($grid["formats"]))&&(array_key_exists($key,$grid["formats"]))){

                                                        switch ($grid["formats"][$key]){
                                                            case "Money":
                                                                ?><input type="text" name="finpt<?=$key?>" id="finpt<?=$key?>"><?php
                                                                break;

                                                            case "PersianDate":
                                                                ?><input type="text" name="finpt<?=$key?>" id="finpt<?=$key?>" onfocus="displayDatePicker(this.id);"><?php
                                                                break;

                                                            case "Date":
                                                                ?><input type="text" name="finpt<?=$key?>" id="finpt<?=$key?>" onfocus="displayDatePicker(this.id);"><?php
                                                                break;
                                                        }
                                                    }else{
                                                        ?>
                                                        <input type="text" name="finpt<?=$key?>" id="finpt<?=$key?>">
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <label for="fbuydate">تا <?=$grid["columns"][$key]?></label>
                                                <?php
                                                    if ((isset($grid["formats"]))&&(array_key_exists($key,$grid["formats"]))){

                                                        switch ($grid["formats"][$key]){
                                                            case "Money":
                                                                ?><input type="text" name="tinpt<?=$key?>" id="tinpt<?=$key?>"><?php
                                                                break;

                                                            case "PersianDate":
                                                                ?><input type="text" name="tinpt<?=$key?>" id="tinpt<?=$key?>" onfocus="displayDatePicker(this.id);"><?php
                                                                break;

                                                            case "Date":
                                                                ?><input type="text" name="tinpt<?=$key?>" id="tinpt<?=$key?>" onfocus="displayDatePicker(this.id);"><?php
                                                                break;
                                                        }
                                                    }else{
                                                        ?>
                                                        <input type="text" name="tinpt<?=$key?>" id="tinpt<?=$key?>">
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        <?php
                                        break;
                                }
                            }

                            if(isset($grid["parameters"])){
                                foreach ($grid["parameters"] as $key =>$value){
                                    ?>
                                    <input type="hidden" id="<?=$key?>" name="<?=$key?>" value="<?=$value?>" />
                                    <?php
                                }
                            }
                            ?>

                            <div class="form-group">
                                <input type="submit" value="جستجو" class="main-btn srchbtn">
                            </div>
                            <?php
                            DrawPaging($grid["name"],isset($grid["excel"]));
                            ?>
                            <?php
                        }
                    ?>


                </form>
            </div>
        </div>
    </div>
    <script>
        $('#<?=$grid["name"]?>Form').submit(function() {
            $(this).attr('action','<?=url(route($grid["route"]))?>');
            // submit the form
            ShowLoader();
            $(this).ajaxSubmit(function(data){
                $('#pnl<?=$grid["name"]?>').parent().parent().html(data);
                HideLoader();
            });
            // return false to prevent normal browser submit and page navigation
            return false;
        });
    </script>
    <?php
}

function ShowGrid($grid){
    DrawHeader($grid);
    DrawBody($grid);
    DrawFooter();
    if(count($grid["data"])==0){
        ?>
        <div style="margin: 0px auto;">موردی یافت نشد!</div>
        <?php
    }
}

function DrawPaging($gname,$have_excel){
    ?>
    <div class="row">
        <div class="partition-1">
            <div class="search_box formblk">
                <?php
                if($have_excel){
                    ?> <div class="form-group">
                        <label for="">دانلود گزارشات</label>
                        <a href="javascript:download('<?=$gname?>Form');" class="excel"></a>
                    </div>
                <?php
                }
                ?>


                <div class="form-group">
                    <label for="">صفحه</label>
                    <select name="page" id="page"  onchange="$('#<?=$gname?>Form').submit();">
                        <?php
                        for ($i=1;$i<25;$i++)
                            echo "<option value='$i'>$i</option>";
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">تعداد رکورد در هر صفحه</label>
                    <select name="pagesize" id="pagesize"  onchange="$('#<?=$gname?>Form').submit();">
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function DrawHeader($grid){
    $columns=$grid["columns"];
    $buttons=array();
    if(isset($grid["buttons"])) {
        $buttons=$grid["buttons"];
    }
    if($grid["is-first"]){
        DrawSerachPannel($grid);
    }

    ?>
    <div class="row">
        <div class="partition-1">
            <div class="tableBox" id="pnl<?=$grid["name"]?>">
                <table>
                    <thead>
                    <?php
                    foreach($columns as $key => $value)
                        echo "<th> $value </th>";
                    foreach($buttons as $key => $value)
                       echo "<th> $key </th>";

                    ?>
                    </thead>
    <?php
}

function DrawFooter(){
    ?>
            </table>
        </div>
    </div>
    <?php
}

function DrawBody($grid){
    $columns=$grid["columns"];
    $buttons=array();
    if(isset($grid["buttons"])) {
        $buttons=$grid["buttons"];
    }
    echo "<tbody>";
    $rownumber=0;
    $sums=array();
    if(isset($grid["sums"])) {
        foreach ($grid["sums"] as $s) {
            $sums[$s] = 0;
        }
    }
    if(!is_array($grid["data"]))
        $grid["data"]=$grid["data"]->toArray();

    foreach ($grid["data"] as $row){
        $primary=$row[$grid["primary"]];
        $rownumber++;
        echo "<tr id=$rownumber>";
        foreach($columns as $key => $value){
            $class="";
            if(array_key_exists($key,$grid["styles"])){
                if(is_array($grid["styles"][$key])){
                    if(array_key_exists($row[$key],$grid["styles"][$key])){
                        $class="class='".$grid["styles"][$key][$row[$key]]."'";
                    }
                }
                else
                    $class="class='".$grid["styles"][$key]."'";
            }

            echo "<td $class >";
            if($key=="#"){
                echo ToPersianNumber($rownumber);
                continue;
            }
            elseif((array_key_exists("lists",$grid))&&(array_key_exists($key,$grid["lists"]))){
                //dd($row[$key]);
                echo $grid["lists"][$key][$row[$key]];
                continue;
            }
            elseif((array_key_exists("combo",$grid))&&array_key_exists($key,$grid["combo"])){
                DrawCombo($grid["combo"][$key],$row[$key],"dd"."_"."$key"."_"."$primary");
                //echo $grid["lists"][$key][$row[$key]];
                continue;
            }
            else{
                if(array_key_exists($key,$grid["formats"])){
                    switch ($grid["formats"][$key]){
                        case "Money":
                            echo ToPersianNumber(number_format($row[$key]));
                            break;
                        case "Number2Digit":
                            echo ToPersianNumber(number_format(round($row[$key],2)));
                            break;
                        case "Date":
                            echo ToDateFormat($row[$key]);
                            break;
                        default:
                            echo ToPersianNumber($row[$key]);
                            break;
                    }
                }
                else
                    echo ToPersianNumber($row[$key]);
            }
            echo "</td>";
            if(isset($sums[$key]))
                $sums[$key]=$sums[$key]+$row[$key];
        }

        foreach($buttons as $key => $value){
            $functions="";
            if(isset($value["functions"])){
                foreach ($value["functions"] as $key2 => $value2){
                   $functions=$functions." "." $key2='$value2'";
                }
            }
            $data_field="";
            if(isset($value["data-fields"])){
                foreach ($value["data-fields"] as $dfs){
                    $data_field.="data-field-$dfs='".$row[$dfs]."'";
                }
            }
            $class="";
            if(is_array($value["class"])){
                $class="class='".$value["class"][$row[$value["class-key"]]]."'";
            }else{
                $class="class='".$value["class"]."'";
            }
            $href="";
            echo "<td $class $data_field $functions>";
            if(isset($value["href"])){
                $href=$value["href"];
                $target="";
                if(isset($value["target"]))
                    $target=" target='".$value["target"]."'";

                echo "<a href='$href' $target ></a>";
            }elseif(isset($value["rout"])){
                $rout_fields=[];
                if(isset($value["data-fields"])){
                    foreach ($value["data-fields"] as $keyr => $valuer){
                        $rout_fields[$keyr]=$row[$valuer];
                    }
                }
                $href=url(route($value["rout"],$rout_fields));
                $target="";
                if(isset($value["target"]))
                    $target=" target='".$value["target"]."'";

                echo "<a href='$href' $target ></a>";
            }


            echo "</td>";
        }


        echo "</tr>";
    }
    if(isset($grid["sums"])) {
        /*$count = count($columns) + count($buttons);
        echo "<tr><td colspan=".$count.">جمع</td></tr>";
*/      echo "<tr class=".'"sum"'.">";
        foreach($columns as $key => $value){
            echo "<td>";
            if(isset($sums[$key]))
                echo ToPersianNumber(number_format(round($sums[$key],2)));
            echo "</td>";
        }
        foreach($buttons as $key => $value){
            echo "<td></td>";
        }
        echo "</tr>";
    }
    echo "</tbody>";
}

function DrawCombo($list,$selected,$id,$have_all=false){
    echo "<select name='$id' id='$id'>";
    if($have_all)
         echo "<option value='all' selected >همه</option>";
    foreach($list as $key => $value){
        if($selected==$key)
            echo "<option value='$key' selected >$value</option>";
        else
            echo "<option value='$key'>$value</option>";
    }
    echo '</select>';
}

function ToPersianNumber($str){
    $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
    $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');

    return str_replace($western_arabic, $eastern_arabic, $str);
}

function ToDateFormat($str){
    $dateFormat = \jDate::forge($str)->format('Y/m/d - H:i:s');
    return ToPersianNumber($dateFormat);
}

 function GridToExcel($grid){
    $pdate=Jalalian::now()->format("Y-m-d");
    /*$arrayData=array();

    foreach ($grid["excel"] as $item){
        $arrayData[]=$grid["columns"][$item];
    }
    $arrayData[]=$grid["excel"];
    foreach ($grid["data"] as $item) {
        $arrayData[]=$grid[$item];
    }*/
    $tmpc=array();
     foreach ($grid["excel"] as $item){
         $tmpc[]=$grid["columns"][$item];
     }
     $arrayData[]=$tmpc;
    //$arrayData[]=$grid["columns"];
    foreach ($grid["data"] as $o) {
        $rw=array();
        foreach ($grid["excel"] as $item){
            if((isset($grid["list"][$item]))||(isset($grid["combo"][$item]))){
                if(isset($grid["combo"][$item]))
                    $rw[]=$grid["combo"][$item][$o[$item]];
                if(isset($grid["list"][$item]))
                    $rw[]=$grid["list"][$item][$o[$item]];
            }
            else
                $rw[]=$o[$item];
        }
        $arrayData[] = $rw;//$o->toArray();
    }
    //header("Content-type: application/force-download");
    ob_end_clean();
    ob_start();
    //store(object $export, string $filePath, string $disk = null, string $writerType = null, $diskOptions = [])
    Excel::store($grid["name"].$pdate, function ($excel) use ($arrayData,$grid) {
    //Excel::create($grid["name"].$pdate, function ($excel) use ($arrayData,$grid) {

        // Set the spreadsheet title, creator, and description
        $excel->setTitle($grid["title"]);
        $excel->setCreator('tikook.com')->setCompany('تیکوک');
        $excel->setDescription($grid["title"]);

        // Build the spreadsheet, passing in the payments array
        $excel->sheet('sheet1', function ($sheet) use ($arrayData) {
            $sheet->fromArray($arrayData, null, 'A1', false, false);
            $sheet->setRightToLeft(true);
            $sheet->setStyle(array(
                'font' => array(
                    'name'      =>  'koodak',
                    'size'      =>  12,
                    'horizontal' => 'center'
                )
            ));
            $sheet->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->row(1, function($row) {
                $row->setBackground('#4abbc9');
                $row->setFontWeight('bold');
                $row->setFontColor('#ffffff');
                $row->setFontSize(13);
            });
        });

    })->download('xlsx');
}
