<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultiMedia extends Model
{
    protected $table = 'multi_medias';

    protected $guarded = [];
}
