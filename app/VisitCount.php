<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitCount extends Model
{
    protected $table = 'visit_counts';

    protected $guarded = [];
}
