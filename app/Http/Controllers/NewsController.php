<?php

namespace App\Http\Controllers;

use App\Category;
use App\MultiMedia;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;

class NewsController extends Controller
{
    public function index()
    {
        return view('news.index');
    }

    public function GetNewsHomeData(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $news = News::where('row_status', '!=', 'updated')
                                        ->offset($offset * $limit)
                                        ->orderBy('last_update_ts', 'DESC')
                                        ->limit($limit)
                                        ->get();

        return ['news' => $news, 'offset' => $request->offset + 1];

    }


    public function NewNewsView(Request $request)
    {
        return view('news.new');
    }


    public function NewNews(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
        ],[
            'title' => 'required|min::3',
            'description' => 'required|min::3',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $news = new News();
        $news->title = $request->title;
        $news->description = $request->description;
        $news->hash_1 = $request->hash_1_blob;
        $news->row_status = 'active';
        $news->last_update_ts = MiliTime();
        $news->p_date = Jalalian::now()->format('Y/m/d');
        $news->save();

        return ['msg' => 'success', 'news' => $news];
    }


    public function ActivateNews(Request $request)
    {
        $news = News::find($request->id);
        if (empty($news)) {
            return ['msg' => 'error', 'error' => 'news not found'];
        }
        if ($request->type == 'active') {
            $news->row_status = 'active';
        }elseif ($request->type == 'deActive') {
            $news->row_status = 'suspended';
        }
        $news->save();

        return ['msg' => 'success', 'news' => $news];
    }


    public function EditNewsView($_id)
    {
        $news = News::find($_id);

        return view('news.edit', compact('news'));
    }


    public function UpdateNews(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
        ],[
            'title' => 'required|min::3',
            'description' => 'required|min::3',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $news = News::find($request->id);
        $news->title = $request->title;
        $news->description = $request->description;
        if (isset($request->hash_1_blob)) {
            $news->hash_1 = $request->hash_1_blob;
        }
        $news->row_status = 'active';
        $news->last_update_ts = MiliTime();
        $news->p_date = Jalalian::now()->format('Y/m/d');
        $news->save();

        return ['msg' => 'success', 'news' => $news];
    }
}
