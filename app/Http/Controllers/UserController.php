<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Message;
use App\MultiMedia;
use App\News;
use App\Order;
use App\Page;
use App\Product;
use App\Transaction;
use App\User;
use App\Visit;
use App\VisitCount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function Authenticate(Request $request)
    {

        $v = Validator::make([
            "email" => $request->email,
            "password" => $request->password,
        ], [
            'email' => 'required',
            'password' => 'required|min:5',
        ], [

        ], [
            'email' => 'email',
            'password' => 'password',
        ]);

        $errorString = implode("<br />", $v->messages()->all());

        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        };

        $credentials = $request->only('email', 'password', 'status');
        if (Auth::attempt($credentials, true)) {
            $contents = "window.location='" . url('/') . "';";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
//            $this->ShowHomePage();
        } else {
            $contents = "ShowMessage('error','رمز عبور وارد شده معتبر نمی باشد.',false,false);";
            $contents .= "HideLoader()";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }
    }

    public function LogOut()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function ShowHomePage()
    {
        $page = Page::where('title', 'index')->first();
        return view('welcome', compact('page'));
    }


    public function HomePageData(Request $request)
    {

        $transactions = Transaction::where('row_status', 'active')
                                                            ->where('new', 1)
                                                            ->get();

        $customers = Customer::where('row_status', 'active')
                                                    ->where('new', 1)
                                                    ->get();

        $orders = Order::where('row_status', 'active')
                                        ->where('new', 1)
                                        ->get();

        $messages = Message::where('row_status', 'active')
                                                    ->where('new', 1)
                                                    ->get();

        $products = Product::where('row_status', 'active')
                                                ->select('*',
                                                    DB::raw("(SELECT COUNT(id) FROM `user_cart` WHERE `products`.`id` = `user_cart`.`product_id`) as orders_count"))
                                                ->orderBy('orders_count', 'DESC')
                                                ->limit(6)
                                                ->get();

        $news = News::where('row_status', 'active')
                                    ->orderBy('last_update_ts', 'DESC')
                                    ->limit(5)
                                    ->get();

        return ['messages' => $messages, 'transactions' => $transactions, 'news' => $news,
                'orders' => $orders, 'customers' => $customers, 'products' => $products];
    }


    public function NewVisit(Request $request)
    {
        $t = date('d-m-Y');
        $dayName = strtolower(date("D", strtotime($t)));
//        dd($dayName);
        $dayNum = strtolower(date("d", strtotime($t)));
//        dd($dayNum);
        $monthName = strtolower(date("M", strtotime($t)));
//        dd($monthName);
        $monthNum = strtolower(date("m", strtotime($t)));
//        dd($monthNum);

        $visit = new Visit();
        $visit->client_ip = $request->ip();
        $visit->client_os = PHP_OS;
        $visit->client_referrer = $request->server('REDIRECT_URL');
        $visit->client_agent = $request->userAgent();
        $visit->p_date = now();
        $visit->day = $dayName;
        $visit->day_num = floor($dayNum);
        $visit->month = $monthName;
        $visit->month_num = floor($monthNum);
        $visit->row_status = 'active';
        $visit->save();

        VisitCount::where('month', $monthName)
                    ->update(['count' => DB::raw('count+1')]);

        return 'ok';
    }


    public function GetVisits(Request $request)
    {
        $visits = VisitCount::all();

        return $visits;
    }


    public function UserProfile(Request $request)
    {
        $user = User::find($request->id);

        return view('templates.profile2', compact('user'));
    }


    public function EditProfileImage()
    {
        return view('templates.imageEdit');
    }


    public function UpdateProfileImage(Request $request)
    {
        $v = Validator::make([
            'image' => $request->image
        ],[
            'image' => 'required|image|max:5000'
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $siteDes = 'C:\xampp\htdocs\fanusSoft\public\img\profiles';

        $ext = $request->image->extension();
        $fileSize = $_FILES['image']['size'];
        $hash = Auth::user()->id . MiliTime() . rand(10000, 99999);

        $moveRes = $request->image->move(public_path()."/img/profiles/", $hash . '.' . $ext);
        if ($moveRes != true) {
            $errorString = '500. uploading failed';
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }
        $local_path = public_path()."/img/profiles/$hash" . '.' . $ext;
        $size = list($width, $height) = getimagesize($local_path);

        // copying file to site dir
        $moveRes = copy($local_path, $siteDes . '\\' . $hash . '.' . $ext);

        $multiMedia = new MultiMedia();
        $multiMedia->owner_id = Auth::user()->id;
        $multiMedia->hash_key = 'img/profiles/' . $hash . '.' . $ext;
        $multiMedia->main_hash = $hash;
        $multiMedia->type = 'avatar';
        $multiMedia->width = $width;
        $multiMedia->height = $height;
        $multiMedia->length = $fileSize;
        $multiMedia->original_extension = $ext;
        $multiMedia->row_status = 'active';
        $multiMedia->last_updated_ts = MiliTime();
        $multiMedia->save();

        $user = User::find(Auth::user()->id);
        $user->profile_hash_key = $multiMedia->hash_key;
        $user->save();
//        $errorString = 'با موفقیت انجام شد';
//        $contents = "ShowMessage('success','" . $errorString . "',false,false);";
//        $contents .= "HideLoader();";
//        $response = Response::make($contents, 200);
//        $response->header('Content-Type', 'application/javascript');
//        return $response;

        return ['hash_key' => $hash];
    }

    public function UpdateUser(Request $request)
    {

        $v = Validator::make([
            'name' => $request->name,
            'email' => $request->email,
            'cellphone' => $request->cellphone,
            'cv' => $request->cv
        ],[
            'name' => 'required|min:3',
            'email' => 'sometimes',
            'cellphone' => 'required|min:10',
            'cv' => 'sometimes'
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->cellphone = $request->cellphone;
        $user->cv = $request->cv;
        $user->last_updated_ts = MiliTime();
        $user->save();

        $errorString = 'با موفقیت انجام شد';
        $contents = "ShowMessage('success','" . $errorString . "',false,false);";
        $contents .= "HideLoader();";
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/javascript');
        return $response;
    }
}
