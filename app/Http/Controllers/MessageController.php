<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Message;
use App\Product;
use App\UserCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;

class MessageController extends Controller
{
    public function Index()
    {
        return view('messages.index');
    }

    public function GetMessages(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $messages = null;
        if ($request->filter == 'all') {
            $messages = Message::where('row_status', 'active');
        } elseif ($request->filter == 'message') {
            $messages = Message::where('row_status', 'active')
                                                            ->where('message_type', 'message');
        }elseif ($request->filter == 'comment') {
            $messages = Message::where('row_status', 'active')
                                                            ->where('message_type', 'comment');
        }
        $messages = $messages->select('messages.*',
                                                            DB::raw("(SELECT `title` FROM `products` WHERE `products`.`id` = `messages`.`product_id`) as p_title"))
                                                        ->offset($offset * $limit)
                                                        ->orderBy('last_update_ts', 'DESC')
                                                        ->limit($limit)
                                                        ->get();

        return ['messages' => $messages, 'offset' => $request->offset + 1];

    }


    public function UpdateMessageSeenStatus(Request $request)
    {
        if (! empty($request->messages)) {
            Message::whereIn('id', $request->messages)
                                ->update([
                                    'new' => 0
                                ]);

            return ['msg' => 'success'];
        }
    }


    public function ReplyMessageView(Request $request)
    {
        $message = Message::find($request->id);
        if ($message->product_id != null) {
            $title = DB::table('products')
                                ->select('title')
                                ->where('id', $message->product_id)
                                ->first()->title;

            $message->p_title = $title;
        }

        return view('messages.reply', compact('message'));
    }


    public function NewMessage(Request $request)
    {

        $v = Validator::make([
            'text' => $request->text,
            'reply_id' => $request->reply_id
        ],[
            'text' => 'required',
            'reply_id' => 'sometimes',
        ]);


        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $message = new Message();
        $message->owner_id = -1;
        $message->product_id = $request->product_id;
        $message->signature = 'تیم پشتیبانی';
        $message->content = $request->text;
        $message->reply_message_id = $request->reply_id;
        $message->message_type = 'comment';
        $message->type = 'reply';
        $message->new = 0;
        $message->row_status = 'active';
        $message->last_update_ts = MiliTime();
        $message->p_date = Jalalian::now()->format('Y/m/d H:i:s');
        $message->save();

        $message->p_title = $request->p_title;

        return ['msg' => 'success', 'message' => $message];
    }


    public function ActivateMessage(Request $request)
    {
        $message = Message::find($request->id);
        if (empty($message)) {
            return ['msg' => 'error', 'error' => 'message not found'];
        }
        if ($request->type == 'active') {
            $message->row_status = 'active';
        }elseif ($request->type == 'deActive') {
            $message->row_status = 'suspended';
        }
        $message->save();

        return ['msg' => 'success', 'message' => $message];
    }

}
