<?php

namespace App\Http\Controllers;

use App\Order;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function Index()
    {
        return view('transactions.index');
    }

    public function GetTransactions(Request $request)
    {
        $trans = Transaction::join('customers', 'customers.id', '=', 'transactions.customer_id')
                                                ->select('transactions.*', 'customers.name', 'customers.last_name')
                                                ->where('transactions.row_status', 'active')
                                                ->get();

        return ['msg' => 'success', 'transactions' => $trans];
    }


    public function ChangeStatus(Request $request)
    {
        $trans = Transaction::find($request->id);

        if (empty($trans)) {
            return ['msg' => 'error', 'error' => 'transaction not found'];
        }

        $trans->status = $request->status;
        $trans->save();

        return ['msg' => 'success', 'transaction' => $trans];
    }


    public function UpdateSeenStatus(Request $request)
    {
        Transaction::whereIn('id', $request->transactions)
            ->update([
                'new' => 0
            ]);

        return ['msg' => 'success'];
    }
}
