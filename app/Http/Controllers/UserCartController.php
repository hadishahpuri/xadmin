<?php

namespace App\Http\Controllers;

use App\Order;
use App\UserCart;
use Illuminate\Http\Request;

class UserCartController extends Controller
{
    public function index()
    {
        return view('orders.index');
    }

    public function GetOrdersHomeData(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $orders = Order::where('row_status', '!=', 'updated')
                                    ->offset($offset * $limit)
                                    ->orderBy('last_update_ts', 'DESC')
                                    ->limit($limit)
                                    ->get();

        return ['orders' => $orders, 'offset' => $request->offset + 1];

    }


}
