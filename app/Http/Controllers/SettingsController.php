<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function index()
    {
        $contact = Page::where('id', 2)
                                        ->first();

        $about = Page::where('id', 3)
                                     ->first();

        return view('settings.index', compact('contact', 'about'));
    }


    public function UpdatePages(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
            'text' => $request->text,
        ],[
            'title' => 'required',
            'description' => 'sometimes',
            'text' => 'required',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $page = Page::find($request->id);

        $page->title = $request->title;
        $page->description = $request->description;
        $page->text = $request->text;
        $page->save();

        return ['msg' => 'success', 'page' => $page];
    }
}
