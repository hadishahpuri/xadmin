<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Mail\NewsLetterEmail;
use App\NewsLetter;
use App\Order;
use App\Product;
use App\UserCart;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function index()
    {
        return view('customers.index');
    }

    public function GetCustomers(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $customers = Customer::whereIn('row_status', ['active', 'suspended'])
                                                        ->select('customers.*',
                                                        DB::raw("(SELECT COUNT(id) FROM `orders` WHERE `customers`.`id` = `orders`.`customer_id`) as orders_count"))
                                                        ->offset($offset * $limit)
                                                        ->orderBy('last_update_ts', 'DESC')
                                                        ->limit($limit)
                                                        ->get();

        return ['customers' => $customers, 'offset' => $request->offset + 1];

    }


    public function SearchCustomers(Request $request)
    {
        $customers = Customer::whereIn('row_status', ['active', 'suspended'])
                                                        ->select('customers.*',
                                                            DB::raw("(SELECT COUNT(id) FROM `orders` WHERE `customers`.`id` = `orders`.`customer_id`) as orders_count"));

        if ($request->last_name != null) {
            $customers = $customers->where('last_name', 'LIKE', '%' . $request->last_name . '%');
        }
        if ($request->cellphone != null) {
            $customers = $customers->where('cellphone', 'LIKE', '%' . $request->cellphone . '%');
        }

        $customers = $customers->orderBy('last_update_ts', 'DESC')
                                                        ->get();

        return ['msg' => 'success', 'customers' => $customers];
    }


    public function UpdateCustomersSeenStatus(Request $request)
    {
        Customer::whereIn('id', $request->customers)
                            ->update([
                               'new' => 0
                            ]);

        return $request->customers;
    }


    public function NewCustomerView(Request $request)
    {
        return view('customers.new');
    }


    public function RegisterNewCustomer(Request $request)
    {

        $v = Validator::make([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'cellphone' => $request->cellphone,
            'postal_code' => $request->postal_code,
        ],[
           'name' => 'required|min::3',
           'last_name' => 'required|min::3',
           'email' => 'sometimes',
           'cellphone' => 'required',
           'postal_code' => 'sometimes|max:15'
        ]);


        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->last_name = $request->last_name;
        $customer->cellphone = $request->cellphone;
        $customer->email = $request->email;
        $customer->ip = $request->ip();
        $customer->postal_code = $request->postal_code;
        $customer->row_status = 'active';
        $customer->last_update_ts = MiliTime();
        $customer->save();

        return ['msg' => 'success', 'customer' => $customer];
    }


    public function ActivateCustomer(Request $request)
    {
        $customer = Customer::find($request->id);
        if (empty($customer)) {
            return ['msg' => 'error', 'customer' => null];
        }

        $customer->row_status = 'active';
        $customer->last_update_ts = MiliTime();
        $customer->save();

        return ['msg' => 'success', 'customer' => $customer];
    }


    public function DeactiveCustomer(Request $request)
    {
        $customer = Customer::find($request->id);
        if (empty($customer)) {
            return ['msg' => 'error', 'customer' => null];
        }

        $customer->row_status = 'suspended';
        $customer->last_update_ts = MiliTime();
        $customer->save();

        return ['msg' => 'success', 'customer' => $customer];
    }


    public function CustomerMoreDetails(Request $request)
    {
        $customer = Customer::find($request->id);
        if (empty($customer)) {
            return ['msg' => 'error', 'error' => 'customer not found'];
        }

        $orders = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
                                    ->select('orders.*', 'customers.name', 'customers.last_name')
                                    ->where('orders.row_status', 'active')
                                    ->where('customers.id', $customer->id)
                                    ->get();

        if (! empty($orders)) {
            foreach ($orders as $order) {
                $order->info = [];
                $order->info = Product::join('user_cart', 'user_cart.product_id', '=', 'products.id')
                                                            ->select('products.title', 'products.hash_1', 'user_cart.count', 'products.price', 'products.row_status')
                                                            ->where('user_cart.order_id', $order->id)
                                                            ->get();
            }
        }

        return view('customers.details', compact('customer', 'orders'));
    }

    public function NewNewsLetterView()
    {
        return view('customers.newsLetter');
    }

    public function NewNewsLetter(Request $request)
    {
//        ini_set('max_execution_time', '300');
        $v = Validator::make([
            'title' => $request->title,
            'type' => $request->type,
            'description' => $request->description,
        ],[
            'title' => 'required|min::3',
            'type' => 'required',
            'description' => 'required',
        ],[
            'required' => ':attribute اجباری است',
        ], [
            'title' => 'عنوان',
            'type' => 'نوع',
            'description' => 'متن خبرنامه',
        ]);

        $errorString = implode("<br />", $v->messages()->all());

        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        };
        $customers = [];
        $emails = [];
        if ($request->type == 'all') {
            $customers = Customer::where('row_status', 'active')
                                                            ->get()->toArray();
            if (! empty($customers)) {
                $emailsArr = Arr::pluck($customers, 'email');
                $emails = Arr::where($emailsArr, function ($value, $key) {
                    if ($value != null && $value != '') {
                        return $value;
                    }
                });
            }
        }else {
            $customers = NewsLetter::where('row_status', 'active')
                                                                ->get()->toArray();
            if (! empty($customers)) {
                $emailsArr = Arr::pluck($customers, 'email');
                $emails = Arr::where($emailsArr, function ($value, $key) {
                    if ($value != null && $value != '') {
                        return $value;
                    }
                });
            }
        }

        $data['title'] = $request->title;
        $data['body'] = $request->description;

        if (! empty($emails)) {
            Mail::to($emails)->send(new NewsLetterEmail($data));
        }

        return ['msg' => 'success'];
    }
}
