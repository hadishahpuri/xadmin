<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\Product;
use App\User;
use App\UserCart;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;

class OrderController extends Controller
{

    public function index()
    {
        return view('orders.index');
    }

    public function GetOrdersHomeData(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $orders = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
                                    ->select('orders.*', 'customers.name', 'customers.last_name')
                                    ->where('orders.row_status', 'active')
                                    ->offset($offset * $limit)
                                    ->orderBy('orders.last_update_ts', 'DESC')
                                    ->limit($limit)
                                    ->get();

        if (! empty($orders)) {
            foreach ($orders as $order) {
                $order->info = [];
                $order->info = Product::join('user_cart', 'user_cart.product_id', '=', 'products.id')
                                                            ->select('products.title', 'products.hash_1', 'user_cart.count', 'products.price', 'products.row_status')
                                                            ->where('user_cart.order_id', $order->id)
                                                            ->get();
            }
        }
        return ['orders' => $orders, 'offset' => $request->offset + 1];

    }



    public function ChangeOrderStatus(Request $request)
    {
        $order = Order::find($request->id);
        if (empty($order)) {
            return ['msg' => 'error', 'error' => 'order not found'];
        }
        $order->status = $request->status;
        $order->save();

        return ['msg' => 'success', 'order' => $order];
    }


    public function NewOrderView()
    {
        $customers = Customer::where('row_status', 'active')->get();

        $products = Product::where('row_status', 'active')->get();

        return view('orders.new', compact('customers', 'products'));
    }



    public function NewOrder(Request $request)
    {
        $v = Validator::make([
            'customer_id' => $request->customer_id,
            'products' => $request->products
        ],[
           'customer_id' => 'required',
           'products' => 'required'
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $products = $request->products;
        $prices = Arr::pluck($products, 'price');
        $totalPrice = 0;
        foreach ($prices as $price) {
            $totalPrice = $totalPrice + $price;
        }

        $order = new Order();
        $order->customer_id = $request->customer_id;
        $order->price = $totalPrice;
        $order->status = $request->status;
        $order->row_status = 'active';
        $order->last_update_ts = MiliTime();
        $order->p_date = Jalalian::now()->format('Y/m/d');
        $order->save();

        foreach ($products as $product) {
            $cart = new UserCart();
            $cart->customer_id = $order->customer_id;
            $cart->order_id = $order->id;
            $cart->product_id = $product['id'];
            $cart->count = $product['count'];
            $cart->price = $product['price'];
            $cart->row_status = 'active';
            $cart->last_update_ts = MiliTime();
            $cart->save();
        }

        return ['msg' => 'success', 'order' => $order];

    }


    public function UpdateSeenStatus(Request $request)
    {
        Order::whereIn('id', $request->orders)
                    ->update([
                        'new' => 0
                    ]);

        return ['msg' => 'success'];
    }
}
