<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\MultiMedia;
use App\Product;
use App\User;
use App\UserCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;

class ProductController extends Controller
{
    public function index()
    {
        return view('products.index');
    }

    public function GetProducts(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $customers = Product::where('row_status', '!=', 'updated')
                                                ->select('products.*',
                                                    DB::raw("(SELECT COUNT(id) FROM `user_cart` WHERE `products`.`id` = `user_cart`.`product_id`) as orders_count"))
                                                ->offset($offset * $limit)
                                                ->orderBy('last_update_ts', 'DESC')
                                                ->limit($limit)
                                                ->get();

        return ['products' => $customers, 'offset' => $request->offset + 1];

    }


    public function NewProductView(Request $request)
    {
        $cats = Category::where('row_status', 'active')
                                        ->get();

        return view('products.new', compact('cats'));
    }


    public function NewProduct(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'price' => $request->price,
        ],[
            'title' => 'required|min::3',
            'description' => 'required|min::3',
            'category_id' => 'required',
            'price' => 'required',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $product = new Product();
        $product->title = $request->title;
        $product->description = $request->description;
        if(isset($request->page_desc)) {
            if ($request->page_desc != null) {
                $product->page_desc = $request->page_desc;
            }else {
                $product->page_desc = $request->title;
            }
        }else {
            $product->page_desc = $request->title;
        }
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        if (isset($request->special_offer)) {
            $product->special_offer = 1;
        }

        $product->hash_1 = $request->hash_1_blob;
        $product->hash_2 = $request->hash_2_blob;
        $product->hash_3 = $request->hash_3_blob;
        $product->row_status = 'active';
        $product->last_update_ts = MiliTime();
        $product->p_date = Jalalian::now()->format('Y/m/d');
        $product->save();

        return ['msg' => 'success', 'product' => $product];
    }


    public function SpecialOffer(Request $request)
    {
        $product = Product::find($request->id);
        if (empty($product)) {
            return ['msg' => 'error', 'error' => 'product not found'];
        }

        if ($product->special_offer == 1) {
            $product->special_offer = 0;
        }elseif ($product->special_offer == 0) {
            $product->special_offer = 1;
        }
        $product->save();

        return ['msg' => 'success', 'product' => $product];
    }


    public function ActivateProduct(Request $request)
    {
        $product = Product::find($request->id);
        if (empty($product)) {
            return ['msg' => 'error', 'error' => 'product not found'];
        }
        if ($request->type == 'active') {
            $product->row_status = 'active';
        }elseif ($request->type == 'deActive') {
            $product->row_status = 'suspended';
        }
        $product->save();

        return ['msg' => 'success', 'product' => $product];
    }


    public function EditProductView($_id)
    {
        $product = Product::find($_id);

        $cats = Category::where('row_status', 'active')
                                             ->get();

        return view('products.edit', compact('product', 'cats'));
    }


    public function UpdateProduct(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'price' => $request->price,
        ],[
            'title' => 'required|min::3',
            'description' => 'required|min::3',
            'category_id' => 'required',
            'price' => 'required',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }


        $product = Product::find($request->id);

        if ($product->price != $request->price) {
            $carts = UserCart::where('product_id', $product->id)
                                            ->where('row_status', 'active')
                                            ->where('order_id', null)
                                            ->get();
            if (! empty($carts)) {
                foreach ($carts as $cart) {
                    $cart->price = $cart->count * $request->price;
                    $cart->save();
                }
            }
        }

        $product->title = $request->title;
        $product->description = $request->description;
        if(isset($request->page_desc)) {
            if ($request->page_desc != null) {
                $product->page_desc = $request->page_desc;
            }else {
                $product->page_desc = $request->title;
            }
        }else {
            $product->page_desc = $request->title;
        }
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        if (isset($request->special_offer)) {
            $product->special_offer = 1;
        }

        if (isset($request->hash_1_blob)) {
            $product->hash_1 = $request->hash_1_blob;
        }
        if (isset($request->hash_2_blob)) {
            $product->hash_2 = $request->hash_2_blob;
        }
        if (isset($request->hash_3_blob)) {
            $product->hash_3 = $request->hash_3_blob;
        }
        $product->row_status = 'active';
        $product->last_update_ts = MiliTime();
        $product->p_date = Jalalian::now()->format('Y/m/d');
        $product->save();

        return ['msg' => 'success', 'product' => $product];
    }







    // ------------------- cats -------------------- //
    // ///////////////////////////////////////
    public function CatsIndex()
    {
        return view('cats.index');
    }

    public function GetCats(Request $request)
    {
        $offset = $request->offset -1;
        $limit = 30;
        $cats = Category::where('row_status', '!=', 'updated')
            ->select('categories.*',
                DB::raw("(SELECT COUNT(id) FROM `products` WHERE `products`.`category_id` = `categories`.`id`) as product_count"))
            ->offset($offset * $limit)
            ->orderBy('last_update_ts', 'DESC')
            ->limit($limit)
            ->get();

        return ['cats' => $cats, 'offset' => $request->offset + 1];

    }


    public function NewCatView(Request $request)
    {
        return view('cats.new');
    }


    public function NewCat(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
        ],[
            'title' => 'required|min::3',
            'description' => 'required|min::3',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $cat = new Category();
        $cat->title = $request->title;
        $cat->description = $request->description;
        $cat->row_status = 'active';
        $cat->last_update_ts = MiliTime();
        $cat->save();

        return ['msg' => 'success', 'cat' => $cat];
    }


    public function ActivateCat(Request $request)
    {
        $cat = Category::find($request->id);
        if (empty($cat)) {
            return ['msg' => 'error', 'error' => 'category not found'];
        }
        if ($request->type == 'active') {
            $cat->row_status = 'active';
        }elseif ($request->type == 'deActive') {
            $cat->row_status = 'suspended';
        }
        $cat->save();

        return ['msg' => 'success', 'cat' => $cat];
    }


    public function EditCatView($_id)
    {
        $cat = Category::find($_id);

        return view('cats.edit', compact('cat'));
    }


    public function UpdateCat(Request $request)
    {
        $v = Validator::make([
            'title' => $request->title,
            'description' => $request->description,
        ],[
            'title' => 'required|min::3',
            'description' => 'required|min::3',
        ]);

        $errorString = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorString . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        $cat = Category::find($request->id);

        $cat->title = $request->title;
        $cat->description = $request->description;
        $cat->row_status = 'active';
        $cat->last_update_ts = MiliTime();
        $cat->save();

        return ['msg' => 'success', 'cat' => $cat];
    }
}
